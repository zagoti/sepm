package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.repositories.ChildRepository;
import at.qe.sepm.skeleton.services.*;

import java.util.Calendar;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link ChildService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ChildServiceTest {
	
	@Autowired
	ChildRepository childRepository;

	@Autowired
	ChildService childService;

	@Autowired
	UserService userService;

	@Autowired
	CaregiverService caregiverService;

	@Autowired
	SiblingService siblingService;

	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testDatainitialization() {
		Assert.assertTrue("Insufficient amount of children initialized for test data source",
				4 <= childService.loadAllChildren().size());
		for (Child child : childService.loadAllChildren()) {
			Assert.assertNotNull("child " + child.getId() + " does not have a creationDate defined",
					child.getCreationDate());
			Assert.assertNotNull("child " + child.getId() + " does not have a firstName defined", child.getFirstName());
			Assert.assertNotNull("child " + child.getId() + " does not have a lastName defined", child.getLastName());

			if ("Child_Id_1".equals(child.getId())) {
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_1")));
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_2")));
				Assert.assertTrue(child.getCaregivers().contains(caregiverService.loadCaregiver("Caregiver_Id_1")));
				Assert.assertTrue(child.getEmergencyContact().equals(userService.loadUser("Parent_Id_1")));
				Assert.assertTrue(child.getInternalSiblings().contains(childService.loadChild("Child_Id_2")));
			}

			else if ("Child_Id_2".equals(child.getId())) {
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_1")));
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_2")));
				Assert.assertTrue(child.getCaregivers().contains(caregiverService.loadCaregiver("Caregiver_Id_2")));
				Assert.assertTrue(child.getEmergencyContact().equals(userService.loadUser("Parent_Id_2")));
				Assert.assertTrue(child.getInternalSiblings().contains(childService.loadChild("Child_Id_1")));
			}

			else if ("Child_Id_3".equals(child.getId())) {
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_3")));
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_4")));
				Assert.assertTrue(child.getCaregivers().contains(caregiverService.loadCaregiver("Caregiver_Id_3")));
				Assert.assertTrue(child.getEmergencyContact().equals(userService.loadUser("Parent_Id_3")));
				Assert.assertTrue(child.getInternalSiblings().contains(childService.loadChild("Child_Id_4")));
			}

			else if ("Child_Id_4".equals(child.getId())) {
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_3")));
				Assert.assertTrue(child.getParents().contains(userService.loadUser("Parent_Id_4")));
				Assert.assertTrue(child.getCaregivers().contains(caregiverService.loadCaregiver("Caregiver_Id_4")));
				Assert.assertTrue(child.getEmergencyContact().equals(userService.loadUser("Parent_Id_4")));
				Assert.assertTrue(child.getInternalSiblings().contains(childService.loadChild("Child_Id_3")));
			}
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testDeleteChild() {
		Child toBeDeletedChild = childService.loadChild("Child_Id_3");
		Assert.assertNotNull("child could not be loaded from test data source", toBeDeletedChild);
		int oldSize = childService.loadAllChildren().size();

		childService.deleteChild(toBeDeletedChild);

		Assert.assertTrue("No child has been deleted after calling ChildService.deleteChild",
				(oldSize - 1) == childService.loadAllChildren().size());
		Child deletedChild = childService.loadChild("Child_Id_3");
		Assert.assertNull("Deleted Child could still be loaded from test data source via ChildService.loadChild",
				deletedChild);

		for (Child remainingChild : childService.loadAllChildren()) {
			Assert.assertNotEquals(
					"Deleted Child could still be loaded from test data source via ChildService.getAllChildren",
					toBeDeletedChild.getId(), remainingChild.getId());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testUpdateChild() {
		Child toBeSavedChild = childService.loadChild("Child_Id_3");

		toBeSavedChild.setFirstName("changed first name");
		childService.addParent(toBeSavedChild, userService.loadUser("Parent_Id_7"));
		childService.saveChild(toBeSavedChild);

		Child freshlyLoadedChild = childService.loadChild("Child_Id_3");
		Assert.assertNotNull("Child_Id_3 could not be loaded from test data source after being saved",
				freshlyLoadedChild);
		Assert.assertEquals("Child_Id_3 does not have the correct firstName attribute stored being saved",
				"changed first name", freshlyLoadedChild.getFirstName());
		Assert.assertTrue("Child_Id_3 does not have the correct parent saved",
				freshlyLoadedChild.getParents().contains(userService.loadUser("Parent_Id_7")));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testCreateChild() {

		Child toBeCreatedChild = new Child();
		Calendar dateWithoutTime = Calendar.getInstance();
		dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
		dateWithoutTime.set(Calendar.MINUTE, 0);
		dateWithoutTime.set(Calendar.SECOND, 0);
		dateWithoutTime.set(Calendar.MILLISECOND, 0);
		toBeCreatedChild.setEnabled(true);
		toBeCreatedChild.setFirstName("New");
		toBeCreatedChild.setLastName("Child");
		toBeCreatedChild.setBirthdate(dateWithoutTime);
		toBeCreatedChild.setRegistrationDate(dateWithoutTime);
		toBeCreatedChild.setGender(Gender.FEMALE);
		childService.saveChild(toBeCreatedChild);
		childService.addParent(toBeCreatedChild, userService.loadUser("Parent_Id_7"));

		Child freshlyCreatedChild = childService.loadChild(toBeCreatedChild.getId());
		Assert.assertNotNull("New child could not be loaded from test data source after being saved",
				freshlyCreatedChild);
		Assert.assertEquals("New child does not have the correct firstName saved", "New",
				freshlyCreatedChild.getFirstName());
		Assert.assertEquals("New child does not have the correct lastName saved", "Child",
				freshlyCreatedChild.getLastName());
		Assert.assertEquals("New child does not have the correct birthdate saved", dateWithoutTime,
				freshlyCreatedChild.getBirthdate());
		Assert.assertEquals("New child does not have the correct registrationDate saved", dateWithoutTime,
				freshlyCreatedChild.getRegistrationDate());
		Assert.assertEquals("New child does not have the correct gender saved", Gender.FEMALE,
				freshlyCreatedChild.getGender());
		Assert.assertTrue("New child does not have the correct parent saved",
				freshlyCreatedChild.getParents().contains(userService.loadUser("Parent_Id_7")));
		Assert.assertNotNull("New child does not have a creationDate defined after being saved",
				freshlyCreatedChild.getCreationDate());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testCreateChildWithServiceMethod() {

		Calendar dateWithoutTime = Calendar.getInstance();
		dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
		dateWithoutTime.set(Calendar.MINUTE, 0);
		dateWithoutTime.set(Calendar.SECOND, 0);
		dateWithoutTime.set(Calendar.MILLISECOND, 0);
		Child toBeCreatedChild = childService.createChild("New", "Child", Gender.FEMALE, dateWithoutTime,
				dateWithoutTime);

		Child freshlyCreatedChild = childService.loadChild(toBeCreatedChild.getId());
		Assert.assertNotNull("New child could not be loaded from test data source after being saved",
				freshlyCreatedChild);
		Assert.assertEquals("New child does not have a the correct firstName attribute stored being saved", "New",
				freshlyCreatedChild.getFirstName());
		Assert.assertEquals("New child does not have a the correct lastName attribute stored being saved", "Child",
				freshlyCreatedChild.getLastName());
		Assert.assertEquals("New child does not have a the correct birthdate attribute stored being saved",
				dateWithoutTime, freshlyCreatedChild.getBirthdate());
		Assert.assertEquals("New child does not have a the correct registrationDate attribute stored being saved",
				dateWithoutTime, freshlyCreatedChild.getRegistrationDate());
		Assert.assertEquals("New child does not have a the correct gender attribute stored being saved", Gender.FEMALE,
				freshlyCreatedChild.getGender());
		Assert.assertNotNull("New child does not have a creationDate defined after being saved",
				freshlyCreatedChild.getCreationDate());
	}

	@DirtiesContext
	@Test(expected = org.springframework.security.access.AccessDeniedException.class)
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testUnauthorizedCreateChildWithServiceMethod() {

		Calendar dateWithoutTime = Calendar.getInstance();
		dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
		dateWithoutTime.set(Calendar.MINUTE, 0);
		dateWithoutTime.set(Calendar.SECOND, 0);
		dateWithoutTime.set(Calendar.MILLISECOND, 0);
		Child toBeCreatedChild = childService.createChild("New", "Child", Gender.FEMALE, dateWithoutTime,
				dateWithoutTime);

		Child freshlyCreatedChild = childService.loadChild(toBeCreatedChild.getId());
	}

	@Test(expected = org.springframework.security.authentication.AuthenticationCredentialsNotFoundException.class)
	public void testUnauthenticatedLoadChildrenByParent() {
		for (@SuppressWarnings("unused")
		Child child : childService.loadChildrenByParent(userService.loadUser("Parent_Id_1"))) {
			Assert.fail("Call to childService.getAllChildren should not work without proper authorization");
		}
	}

	@Test(expected = org.springframework.security.access.AccessDeniedException.class)
	@WithMockUser(username = "parent3@gmx.at", authorities = { "PARENT" })
	public void testUnauthorizedLoadChildrenByParent() {
		@SuppressWarnings("unused")
		List<Child> child = childService.loadChildrenByParent(userService.loadUser("2"));
	}

	@WithMockUser(username = "parent3@gmx.at", authorities = { "PARENT" })
	public void testAuthorizedLoadChildrenByParent() {
		List<Child> children = childService.loadChildrenByParent(userService.loadUser("3"));
		Assert.assertEquals("Call to childService.getAllChildren should not work without proper authorization", 1,
				children.size());
	}

	@Test(expected = org.springframework.security.access.AccessDeniedException.class)
	@WithMockUser(username = "parent2@gmx.at", authorities = { "EMPLOYEE" })
	public void testUnauthorizedDeleteChild() {
		Child child = childService.loadChild("Child_Id_2");
		Assert.assertEquals("Call to childService.loadChild returned wrong child", "Child_Id_2", child.getId());
		childService.deleteChild(child);
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testAddParent() {
		Child child = childService.loadChild("Child_Id_1");
		User parent = userService.loadUser("Parent_Id_8");

		childService.addParent(child, parent);
		Assert.assertTrue("The child-parent relation wasn't saved",
				childService.loadChild("Child_Id_1").getParents().contains(userService.loadUser("Parent_Id_8")));

		child = new Child("Firstname", "Lastname", Gender.FEMALE, Calendar.getInstance(), Calendar.getInstance());
		parent = userService.createUser("asdf", "asdf", "asdf", "asdf");
		childService.saveChild(child);
		userService.saveUser(parent);

		childService.addParent(child, parent);
		Assert.assertTrue("The child-parent relation wasn't saved using newly created models",
				childService.loadChild(child.getId()).getParents().contains(userService.loadUser(parent.getId())));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testRemoveParent() {
		Child child = childService.loadChild("Child_Id_1");
		User parent = userService.loadUser("Parent_Id_1");

		Assert.assertTrue("There was no child-parent relation before removing it",
				childService.loadChild(child.getId()).getParents().contains(userService.loadUser(parent.getId())));
		childService.removeParent(child, parent);
		Assert.assertFalse("The child-parent relation wasn't removed",
				childService.loadChild(child.getId()).getParents().contains(userService.loadUser(parent.getId())));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "ADMIN" })
	public void testAddCaregiver() {
		Child child = childService.loadChild("Child_Id_1");
		Caregiver caregiver = caregiverService.loadCaregiver("Caregiver_Id_1");

		childService.addCaregiver(child, caregiver);
		Assert.assertTrue("The child-caregiver relation wasn't saved", childService.loadChild(child.getId())
				.getCaregivers().contains(caregiverService.loadCaregiver(caregiver.getId())));

		child = new Child("Firstname", "Lastname", Gender.FEMALE, Calendar.getInstance(), Calendar.getInstance());
		caregiver = new Caregiver();
		caregiver.setConfirmed(true);
		caregiver.setFirstName("New");
		caregiver.setLastName("Caregiver");
		caregiver.setEmail("new-email@whatever.wherever");
		caregiver.setPrivatePhone("+12 345 67890");
		caregiver.setBusinessPhone("+12 345 67892");
		childService.saveChild(child);
		caregiverService.saveCaregiver(caregiver);

		childService.addCaregiver(child, caregiver);
		Assert.assertTrue("The child-caregiver relation wasn't saved using newly created models", childService
				.loadChild(child.getId()).getCaregivers().contains(caregiverService.loadCaregiver(caregiver.getId())));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testRemoveCaregiver() {
		Child child = childService.loadChild("Child_Id_1");
		Caregiver caregiver = caregiverService.loadCaregiver("Caregiver_Id_1");

		Assert.assertTrue("There was no child-caregiver relation before removing it", childService
				.loadChild(child.getId()).getCaregivers().contains(caregiverService.loadCaregiver(caregiver.getId())));
		childService.removeCaregiver(child, caregiver);
		Assert.assertFalse("The child-caregiver relation wasn't removed", childService.loadChild(child.getId())
				.getCaregivers().contains(caregiverService.loadCaregiver(caregiver.getId())));
	}

	@DirtiesContext
	@Test(expected = org.springframework.security.access.AccessDeniedException.class)
	@WithMockUser(username = "parent5@gmx.at", authorities = { "PARENT" })
	public void testAddCaregiverUnauthorized() {
		Child child = childService.loadChild("Child_Id_1");
		Caregiver caregiver = caregiverService.loadCaregiver("Caregiver_Id_1");

		childService.addCaregiver(child, caregiver);
		Assert.assertTrue("The child-caregiver relation wasn't saved", childService.loadChild(child.getId())
				.getCaregivers().contains(caregiverService.loadCaregiver(caregiver.getId())));

		child = new Child("Firstname", "Lastname", Gender.FEMALE, Calendar.getInstance(), Calendar.getInstance());
		caregiver = new Caregiver();
		caregiver.setConfirmed(true);
		caregiver.setFirstName("New");
		caregiver.setLastName("Caregiver");
		caregiver.setEmail("new-email@whatever.wherever");
		caregiver.setPrivatePhone("+12 345 67890");
		caregiver.setBusinessPhone("+12 345 67892");
		childService.saveChild(child);
		caregiverService.saveCaregiver(caregiver);

		childService.addCaregiver(child, caregiver);
		Assert.fail("Call to childService.addCaregiver should not work without proper authorization");
	}

	@Test(expected = org.springframework.security.access.AccessDeniedException.class)
	@WithMockUser(username = "parent5@gmx.at", authorities = { "PARENT" })
	public void testRemoveCaregiverUnauthorized() {
		Child child = childService.loadChild("Child_Id_1");
		Caregiver caregiver = caregiverService.loadCaregiver("Caregiver_Id_1");

		Assert.assertTrue("There was no child-caregiver relation before removing it", childService
				.loadChild(child.getId()).getCaregivers().contains(caregiverService.loadCaregiver(caregiver.getId())));
		childService.removeCaregiver(child, caregiver);
		Assert.fail("Call to childService.removeCaregiver should not work without proper authorization");
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testAddInternalSibling() {
		Child child1 = childService.loadChild("Child_Id_1");
		Child child2 = childService.loadChild("Child_Id_2");

		Assert.assertTrue(child1.getId() + " and " + child2.getId() + " should already be siblings",
				child1.getInternalSiblings().contains(child2) && child2.getInternalSiblings().contains(child1));

		Child child3 = childService.loadChild("Child_Id_3");
		childService.addInternalSiblingRelation(child1, child3);
		Assert.assertTrue("childService.addInternalSiblingRelation didn't add all relations",
				childService.loadChild(child3.getId()).getInternalSiblings().contains(child1));
		Assert.assertTrue("childService.addInternalSiblingRelation didn't add all relations",
				childService.loadChild(child3.getId()).getInternalSiblings().contains(child2));
		Assert.assertTrue("childService.addInternalSiblingRelation didn't add all relations",
				childService.loadChild(child1.getId()).getInternalSiblings().contains(child3));
		Assert.assertTrue("childService.addInternalSiblingRelation didn't add all relations",
				childService.loadChild(child2.getId()).getInternalSiblings().contains(child3));

		Child child4 = new Child("Firstname", "Lastname", Gender.FEMALE, Calendar.getInstance(),
				Calendar.getInstance());
		childService.saveChild(child4);

		childService.addInternalSiblingRelation(childService.loadChild(child1.getId()), child4);
		Assert.assertTrue(
				"childService.addInternalSiblingRelation didn't add all relations (including a newly created child)",
				childService.loadChild(child1.getId()).getInternalSiblings().contains(child4));
		Assert.assertTrue(
				"childService.addInternalSiblingRelation didn't add all relations (including a newly created child)",
				childService.loadChild(child2.getId()).getInternalSiblings().contains(child4));
		Assert.assertTrue(
				"childService.addInternalSiblingRelation didn't add all relations (including a newly created child)",
				childService.loadChild(child3.getId()).getInternalSiblings().contains(child4));

		Assert.assertTrue(
				"childService.addInternalSiblingRelation didn't add all relations (including a newly created child)",
				childService.loadChild(child4.getId()).getInternalSiblings().contains(child1));
		Assert.assertTrue(
				"childService.addInternalSiblingRelation didn't add all relations (including a newly created child)",
				childService.loadChild(child4.getId()).getInternalSiblings().contains(child2));
		Assert.assertTrue(
				"childService.addInternalSiblingRelation didn't add all relations (including a newly created child)",
				childService.loadChild(child4.getId()).getInternalSiblings().contains(child3));

	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testRemoveInternalSibling() {
		Child child1 = childService.loadChild("Child_Id_1");
		Child child2 = childService.loadChild("Child_Id_2");

		Assert.assertTrue("There was no sibling relation before removing it", childService.loadChild(child1.getId())
				.getInternalSiblings().contains(childService.loadChild(child2.getId())));
		childService.removeInternalSiblingRelation(child1, child2);
		Assert.assertFalse("The sibling relation wasn't removed", childService.loadChild(child1.getId())
				.getInternalSiblings().contains(childService.loadChild(child2.getId())));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testAddExternalSibling() {
		Child child = childService.loadChild("Child_Id_1");
		Sibling sibling = siblingService.loadSibling("Sibling_Id_2");

		childService.addExternalSiblingRelation(child, sibling);
		Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
				childService.loadChild(child.getId()).getExternalSiblings().contains(sibling));
		for (Child c : child.getInternalSiblings())
			Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
					childService.loadChild(c.getId()).getExternalSiblings().contains(sibling));

		childService.addExternalSiblingRelation(childService.loadChild(child.getId()), sibling);
		Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
				childService.loadChild(child.getId()).getExternalSiblings().contains(sibling));
		for (Child c : child.getInternalSiblings())
			Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
					childService.loadChild(c.getId()).getExternalSiblings().contains(sibling));

		Sibling sibling1 = new Sibling("Firstname", "Lastname", Calendar.getInstance());
		siblingService.saveSibling(sibling1);

		childService.addExternalSiblingRelation(childService.loadChild(child.getId()), sibling1);
		Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
				childService.loadChild(child.getId()).getExternalSiblings().contains(sibling1));
		for (Child c : child.getInternalSiblings())
			Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
					childService.loadChild(c.getId()).getExternalSiblings().contains(sibling1));

	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testRemoveExternalSibling() {
		Child child = childService.loadChild("Child_Id_1");
		Sibling sibling = siblingService.loadSibling("Sibling_Id_1");

		Assert.assertTrue("There was no sibling relation before removing it", childService.loadChild(child.getId())
				.getExternalSiblings().contains(siblingService.loadSibling(sibling.getId())));
		childService.removeExternalSiblingRelation(child, sibling);
		Assert.assertFalse("The sibling relation wasn't removed", childService.loadChild(child.getId())
				.getExternalSiblings().contains(siblingService.loadSibling(sibling.getId())));
		for (Child c : child.getInternalSiblings())
			Assert.assertFalse("The sibling relation wasn't removed", childService.loadChild(c.getId())
					.getExternalSiblings().contains(siblingService.loadSibling(sibling.getId())));

	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testAddAllExternalSiblings() {
		Child child = childService.loadChild("Child_Id_1");
		Set<Sibling> siblings = new HashSet<>();
		siblings.add(siblingService.loadSibling("Sibling_Id_2"));
		siblings.add(siblingService.loadSibling("Sibling_Id_3"));
		siblings.add(siblingService.loadSibling("Sibling_Id_4"));

		childService.addAllExternalSiblingRelations(child, siblings);
		Assert.assertTrue("childService.addAllExternalSiblingRelations didn't add all relations",
				childService.loadChild(child.getId()).getExternalSiblings().containsAll(siblings));
		for (Child c : child.getInternalSiblings())
			Assert.assertTrue("childService.addAllExternalSiblingRelations didn't add all relations",
					childService.loadChild(c.getId()).getExternalSiblings().containsAll(siblings));

		Sibling sibling1 = new Sibling("Firstname", "Lastname", Calendar.getInstance());
		siblingService.saveSibling(sibling1);
		siblings.add(sibling1);

		child = childService.addAllExternalSiblingRelations(child, siblings);
		Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
				childService.loadChild(child.getId()).getExternalSiblings().containsAll(siblings));
		for (Child c : child.getInternalSiblings())
			Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
					childService.loadChild(c.getId()).getExternalSiblings().containsAll(siblings));

		// Try to add them a second time
		child = childService.addAllExternalSiblingRelations(child, siblings);
		Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
				childService.loadChild(child.getId()).getExternalSiblings().containsAll(siblings));
		for (Child c : child.getInternalSiblings())
			Assert.assertTrue("childService.addExternalSiblingRelation didn't add all relations",
					childService.loadChild(c.getId()).getExternalSiblings().containsAll(siblings));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testLoadChildByName() {
		Child child = childService.loadChildByName("Child_1_FN Child_1_LN").get(0);
		Assert.assertTrue(child.getFirstName().equals("Child_1_FN"));
		Assert.assertTrue(child.getLastName().equals("Child_1_LN"));
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testLoadChildWhileForcingAttendances() {
		Child child2 = childRepository.findFirstByIdAndFetchAttendancesEagerly("Child_Id_1");
		Assert.assertEquals(1, child2.getAttendances().size());
	}
}
