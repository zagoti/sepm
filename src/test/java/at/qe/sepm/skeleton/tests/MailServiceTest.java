package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.services.MailService;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link MailService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class MailServiceTest {
    @Autowired
    MailService mailService;

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testMail() throws AddressException, MessagingException {
    	String[] array = { "alin.porcic@gmail.com" };
    	mailService.SendMail(array[0], "test", "test");
    	mailService.SendMail(array, "test", "test");
    }
}
