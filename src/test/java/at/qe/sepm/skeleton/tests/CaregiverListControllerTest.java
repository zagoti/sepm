package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.controllers.CaregiverListController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Testing for {@link CaregiverListController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class CaregiverListControllerTest {

	@Autowired
	UserService userService;

	@Autowired
	CaregiverListController caregiverListController;


	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = { "PARENT" })
	public void testCaregiversOfParent() {
		User testParent = userService.loadUser("Parent_Id_1");
		Assert.assertEquals("Not correct numbers of caregivers", caregiverListController.getCaregiversOfParent(testParent).size(), 2);
	}



}
