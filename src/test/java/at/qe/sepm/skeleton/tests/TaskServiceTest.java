package at.qe.sepm.skeleton.tests;

import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.TaskService;
import at.qe.sepm.skeleton.services.UserService;
import org.junit.Assert;

/**
 * Testing for {@link TaskService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class TaskServiceTest {

	@Autowired
	TaskService taskService;

	@Autowired
	UserService userService;

	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE",  "ADMIN"})
	public void testCreateTask() {
		User parent = userService.createUser("test@test.at", "test_fn", "test_ln", "passwd");
		Calendar infoDate = Calendar.getInstance();
		infoDate.add(Calendar.DATE, 1);
		Calendar deadlineTo = infoDate;
		deadlineTo.add(Calendar.DATE, 1);
		Calendar deadlineFrom = deadlineTo;
		deadlineFrom.add(Calendar.DATE, 1);
		taskService.createTask(parent, "testSubject", "testDescription", infoDate, deadlineFrom, deadlineTo);

		infoDate.set(Calendar.MILLISECOND, 0);
		infoDate.set(Calendar.SECOND, 0);
		infoDate.set(Calendar.MINUTE, 0);
		infoDate.set(Calendar.HOUR_OF_DAY, 0);
		deadlineFrom.set(Calendar.MILLISECOND, 0);
		deadlineFrom.set(Calendar.SECOND, 0);
		deadlineFrom.set(Calendar.MINUTE, 0);
		deadlineFrom.set(Calendar.HOUR_OF_DAY, 0);
		deadlineTo.set(Calendar.MILLISECOND, 0);
		deadlineTo.set(Calendar.SECOND, 0);
		deadlineTo.set(Calendar.MINUTE, 0);
		deadlineTo.set(Calendar.HOUR_OF_DAY, 0);
		List<Task> tasks = taskService.loadTasksByParentAndDeadline(parent, deadlineFrom);
		for(Task task : tasks) {
			Assert.assertEquals("Parent was not saved correctly in the newly created Task", parent.getId(), task.getParent().getId());
			Assert.assertEquals("Subject was not saved correctly in the newly created Task", "testSubject", task.getSubject());
			Assert.assertEquals("Description was not saved correctly in the newly created Task", "testDescription", task.getDescription());
			Assert.assertEquals("InfoDate was not saved correctly in the newly created Task", infoDate.getTime(), task.getInfoDate().getTime());
			Assert.assertEquals("DeadlineFrom was not saved correctly in the newly created Task", deadlineFrom.getTime(), task.getDeadlineFrom().getTime());
			Assert.assertEquals("DeadlineTo was not saved correctly in the newly created Task", deadlineTo.getTime(), task.getDeadlineTo().getTime());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void loadTask() {
		Task task = taskService.loadTask("Task_Id_3");
		Assert.assertEquals("Task was not correctly loaded from the DB", "Task 3 Subject", task.getSubject());
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void testLoadUncompletedTasks() {
		List<Task> tasks = taskService.loadUncompletedTasks();
		Assert.assertEquals(3, tasks.size());
		for(Task task : tasks) {
			Assert.assertFalse("Task has already been completed", task.isCompleted());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void testLoadActiveTasks() {
		Calendar now = Calendar.getInstance();
		List<Task> tasks = taskService.loadActiveTasks();
		Assert.assertEquals(2, tasks.size());
		for(Task task : tasks) {
			Assert.assertTrue("Task shouldn't be active because of its InfoDate", now.after(task.getInfoDate()));
			Assert.assertFalse("Task shouldn't be active because it's already been completed", task.isCompleted());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void testLoadTasksByParent() {
		List<Task> tasks = taskService.loadTasksByParent(userService.loadUser("Parent_Id_1"));
		Assert.assertEquals(3, tasks.size());
		for(Task task : tasks) {
			Assert.assertEquals("Task was not correctly loaded from the DB", "A Description", task.getDescription());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void testLoadActiveTasksByParent() {
		List<Task> tasks = taskService.loadActiveTasksByParent(userService.loadUser("Parent_Id_1"));
		Assert.assertEquals(1, tasks.size());
		for(Task task : tasks) {
			Assert.assertEquals("Task was not correctly loaded from the DB", "Task_Id_1", task.getId());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void testLoadTasksByParentAndDeadline() {
		Calendar deadline = Calendar.getInstance();
		deadline.set(Calendar.YEAR, 2017);
		deadline.set(Calendar.MONTH, 5);
		deadline.set(Calendar.DATE, 30);
		deadline.set(Calendar.HOUR_OF_DAY, 0);
		deadline.set(Calendar.MINUTE, 0);
		deadline.set(Calendar.SECOND, 0);
		deadline.set(Calendar.MILLISECOND, 0);
		List<Task> tasks = taskService.loadTasksByParentAndDeadline(userService.loadUser("Parent_Id_1"), deadline);
		Assert.assertEquals(2, tasks.size());
		for(Task task : tasks) {
			Assert.assertEquals("Task was not correctly loaded from the DB", "A Subject", task.getSubject());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void testSaveTask() {
		List<Task> tasks = taskService.loadActiveTasksByParent(userService.loadUser("Parent_Id_1"));
		for(Task task : tasks) {
			task.setDescription("Test");
			taskService.saveTask(task);
		}

		List<Task> results = taskService.loadActiveTasksByParent(userService.loadUser("Parent_Id_1"));
		for(Task task : results) {
			Assert.assertEquals("Task was not correctly saved in the DB", "Test", task.getDescription());
		}
	}

	@DirtiesContext
	@Test
	@WithMockUser(username = "parent1@gmx.at", authorities = {"PARENT"})
	public void testCompleteTask() {
		List<Task> tasks = taskService.loadActiveTasksByParent(userService.loadUser("Parent_Id_1"));
		for(Task task : tasks) {
			taskService.completeTask(task);
		}

		List<Task> results = taskService.loadActiveTasksByParent(userService.loadUser("Parent_Id_1"));
		Assert.assertEquals("Some Tasks where not correctly completed", 0, results.size());
	}
}
