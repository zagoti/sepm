package at.qe.sepm.skeleton.tests;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.ui.beans.DayPlanTimeConfigurationBean;

/**
 * Testing for {@link DayPlanTimeConfigurationBean}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class DayPlanTimeConfigurationBeanTest {

    @Autowired
    private DayPlanTimeConfigurationBean dayPlanTimeConfigurationBean;
    
    @DirtiesContext
    @Test
    public void testGetDay() {
        dayPlanTimeConfigurationBean.init();
        Date start = Calendar.getInstance().getTime();
        Date day = dayPlanTimeConfigurationBean.getDay();
        Assert.assertTrue("The generated date is before the logical timespan", day.equals(start) || day.after(start));
        Date end = Calendar.getInstance().getTime();
        Assert.assertTrue("The generated date is after the logical timespan", day.equals(end) || day.before(end));
    }
    
    @DirtiesContext
    @Test
    public void testGetDayAsCalendar() {
        dayPlanTimeConfigurationBean.setToday();
        Calendar start = Calendar.getInstance();
        Calendar day = dayPlanTimeConfigurationBean.getDayAsCalendar();
        Assert.assertTrue("The generated calendar is before the logical timespan", day.equals(start) || day.after(start));
        Calendar end = Calendar.getInstance();
        Assert.assertTrue("The generated calendar is after the logical timespan", day.equals(end) || day.before(end));
    }
    
    @DirtiesContext
    @Test
    public void testSwitchDay() {
        Calendar day = Calendar.getInstance();
        dayPlanTimeConfigurationBean.setDay(day.getTime());
        
        Calendar nextDay = Calendar.getInstance();
        nextDay.setTime(day.getTime());
        Calendar prevDay = Calendar.getInstance();
        prevDay.setTime(day.getTime());
        
        nextDay.add(Calendar.DATE, 1);
        dayPlanTimeConfigurationBean.setNextDay();
        Assert.assertTrue("Next day couldn't be generated", dayPlanTimeConfigurationBean.getDay().equals(nextDay.getTime()));
        
        dayPlanTimeConfigurationBean.setPrevDay();
        Assert.assertTrue("Previous day couldn't be generated", dayPlanTimeConfigurationBean.getDay().equals(prevDay.getTime()));
    }

}
