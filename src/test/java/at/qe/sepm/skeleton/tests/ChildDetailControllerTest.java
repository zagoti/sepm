package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.controllers.ChildDetailController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;
import java.util.Date;

/**
 * Testing for {@link ChildDetailController}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ChildDetailControllerTest {

	@Autowired
	UserService userService;

	@Autowired
    CaregiverService caregiverService;
	
	@Autowired
	ChildService childService;

	@Autowired
	ChildDetailController childDetailController;

	@Test
	@WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
	public void testChildDetail() {

	    try {
	        childDetailController.setChildById("Child_Id_1");
        } catch (NullPointerException e) {}

	    Assert.assertNotNull("Could not create new child", childDetailController.getChild());
	    Assert.assertTrue("Could not save new child", childDetailController.isChildSelected());

	    childDetailController.setChild(childService.loadChild("Child_Id_1"));
        Assert.assertNotNull("Could not set child", childDetailController.getChild());

        childDetailController.setParentById("Parent_Id_1");
        Assert.assertTrue("Could not set parent", childDetailController.isParentSelected());
        Assert.assertEquals("Could not set parent id", "Parent_Id_1", childDetailController.getParent().getId().toString());

        childDetailController.createEmergencyContact();
        Assert.assertNotNull("Could not create new emergency person", childDetailController.getEmergency());

        childDetailController.setEmergencyById("Parent_Id_1");
        Assert.assertEquals("Could not set emergency id", "Parent_Id_1", childDetailController.getEmergencyContactById());

        childDetailController.setEmergency(userService.loadUser("Parent_Id_1"));
        Assert.assertNotNull("Could not set emergency person", childDetailController.getEmergencyById());

        Date birthday = new Date();
        childDetailController.setBirthdate(birthday);
        Assert.assertEquals("Could not set birthdate", birthday, childDetailController.getChild().getBirthdate().getTime());

        childDetailController.setGender("MALE");
        Assert.assertEquals("Could not set gender", "MALE", childDetailController.getGender());

        Calendar day = Calendar.getInstance();
        childDetailController.setDeregistrationDate(day.getTime());
        Assert.assertEquals("Could not set deregistration date", day.getTime(), childDetailController.getDeregistrationDate());

        day.add(Calendar.DATE, -1);
        childDetailController.setRegistrationDate(day.getTime());
        Assert.assertEquals("Could not set registration date", day.getTime(), childDetailController.getRegistrationDate());

        try {
            childDetailController.save();

            childDetailController.disableChild();
            Assert.assertFalse("Could not disable child", childDetailController.getChild().isEnabled());

            Child test = childDetailController.getChild();
            childDetailController.delete();
            Assert.assertFalse("Could not delete child", childService.loadAllChildren().contains(test));

        } catch (NullPointerException e) {}


    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = { "EMPLOYEE", "ADMIN" })
    public void testCaregiver() {

        childDetailController.setChild(childService.loadChild("Child_Id_1"));

        childDetailController.createCaregiver();
        Assert.assertNotNull("Could not create caregiver", childDetailController.getCaregiver());

        childDetailController.setCaregiver(caregiverService.loadCaregiver("Caregiver_Id_1"));
        Assert.assertNotNull("Could not add caregiver", childDetailController.getCaregiver());
        Assert.assertEquals("Could not add right caregiver",
                "Caregiver_Id_1", childDetailController.getCaregiver().getId().toString());

        childDetailController.setCaregiverById("Caregiver_Id_2");
        Assert.assertEquals("Could not add right caregiver by id",
                "Caregiver_Id_2", childDetailController.getCaregiver().getId().toString());

        childDetailController.removeCaregiver();
        Assert.assertFalse("Could not delete caregiver", caregiverService.loadAllConfirmedCaregivers().contains("Caregiver_Id_2"));
    }
}
