package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collection;

/**
 * Testing for {@link UserService}.
 */
@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"ADMIN"})
    public void testDatainitialization() {
        Assert.assertTrue("Insufficient amount of users initialized for test data source", 3 <= userService.loadAllUsers().size());
        for (User user : userService.loadAllUsers()) {
            if ("1".equals(user.getId())) {
                Assert.assertTrue("User \"admin\" does not have role ADMIN", user.getRoles().contains(UserRole.ADMIN));
                Assert.assertNotNull("User \"admin\" does not have a creationDate defined", user.getCreationDate());
            } else if ("2".equals(user.getId())) {
                Assert.assertTrue("User \"user1\" does not have role EMPLOYEE", user.getRoles().contains(UserRole.EMPLOYEE));
                Assert.assertNotNull("User \"user1\" does not have a createDate defined", user.getCreationDate());
            } else if ("3".equals(user.getId())) {
                Assert.assertTrue("User \"user2\" does not have role PARENT", user.getRoles().contains(UserRole.PARENT));
                Assert.assertNotNull("User \"user2\" does not have a createDate defined", user.getCreationDate());
            }
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"ADMIN"})
    public void testDeleteUser() {
        User toBeDeletedUser = userService.loadUser("Parent_Id_1");
        Assert.assertNotNull("User could not be loaded from test data source", toBeDeletedUser);
        int oldSize = userService.loadAllUsers().size();

        userService.deleteUser(toBeDeletedUser);

        Assert.assertTrue("No user has been deleted after calling UserService.deleteUser", (oldSize - 1) == userService.loadAllUsers().size());
        User deletedUser = userService.loadUser("Parent_Id_1");
        Assert.assertNull("Deleted User could still be loaded from test data source via UserService.loadUser", deletedUser);

        for (User remainingUser : userService.loadAllUsers()) {
            Assert.assertNotEquals("Deleted User could still be loaded from test data source via UserService.getAllUsers", toBeDeletedUser.getId(), remainingUser.getId());
        }
        
        toBeDeletedUser = userService.loadUser("Parent_Id_2");
        Assert.assertNotNull("User could not be loaded from test data source", toBeDeletedUser);

        userService.deleteUser(toBeDeletedUser);

        Assert.assertTrue("No user has been deleted after calling UserService.deleteUser", (oldSize - 2) == userService.loadAllUsers().size());
        deletedUser = userService.loadUser("Parent_Id_2");
        Assert.assertNull("Deleted User could still be loaded from test data source via UserService.loadUser", deletedUser);

        for (User remainingUser : userService.loadAllUsers()) {
            Assert.assertNotEquals("Deleted User could still be loaded from test data source via UserService.getAllUsers", toBeDeletedUser.getId(), remainingUser.getId());
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"ADMIN"})
    public void testUpdateUser() {
        User toBeSavedUser = userService.loadUser("3");

        toBeSavedUser.setEmail("changed-email@whatever.wherever");
        userService.saveUser(toBeSavedUser);

        User freshlyLoadedUser = userService.loadUser("3");
        Assert.assertNotNull("User1 could not be loaded from test data source after being saved", freshlyLoadedUser);
        Assert.assertEquals("User \"user1\" does not have a the correct email attribute stored being saved", "changed-email@whatever.wherever", freshlyLoadedUser.getEmail());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin@gmx.at", authorities = {"EMPLOYEE"})
    public void testCreateUser() {

    	User toBeCreatedUser = userService.createUser("new-email@whatever.wherever", "New", "User", "passwd");
        toBeCreatedUser.setPrivatePhone("+12 345 67890");
        toBeCreatedUser.setBusinessPhone("+12 345 67892");
        toBeCreatedUser.setRoles(Sets.newSet(UserRole.EMPLOYEE, UserRole.PARENT));
        userService.saveUser(toBeCreatedUser);

        User freshlyCreatedUser = userService.loadUser(toBeCreatedUser.getId());
        Assert.assertNotNull("New user could not be loaded from test data source after being saved", freshlyCreatedUser);
        Assert.assertTrue("New user does not have a the correct password attribute stored being saved", new BCryptPasswordEncoder().matches("passwd", freshlyCreatedUser.getPassword()));
        Assert.assertEquals("New user does not have a the correct firstName attribute stored being saved", "New", freshlyCreatedUser.getFirstName());
        Assert.assertEquals("New user does not have a the correct lastName attribute stored being saved", "User", freshlyCreatedUser.getLastName());
        Assert.assertEquals("New user does not have a the correct email attribute stored being saved", "new-email@whatever.wherever", freshlyCreatedUser.getEmail());
        Assert.assertEquals("New user does not have a the correct phone attribute stored being saved", "+12 345 67890", freshlyCreatedUser.getPrivatePhone());
        Assert.assertEquals("New user does not have a the correct phone attribute stored being saved", "+12 345 67892", freshlyCreatedUser.getBusinessPhone());
        Assert.assertTrue("New user does not have role MANAGER", freshlyCreatedUser.getRoles().contains(UserRole.PARENT));
        Assert.assertTrue("New user does not have role EMPLOYEE", freshlyCreatedUser.getRoles().contains(UserRole.EMPLOYEE));
        Assert.assertNotNull("New user does not have a createDate defined after being saved", freshlyCreatedUser.getCreationDate());
    }

    @Test(expected = org.springframework.security.authentication.AuthenticationCredentialsNotFoundException.class)
    public void testUnauthenticatedLoadUsers() {
        for (@SuppressWarnings("unused") User user : userService.loadAllUsers()) {
            Assert.fail("Call to userService.getAllUsers should not work without proper authorization");
        }
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(authorities = {"PARENT"})
    public void testUnauthorizedLoadUsers() {
        for (@SuppressWarnings("unused") User user : userService.loadAllUsers()) {
            Assert.fail("Call to userService.getAllUsers should not work without proper authorization");
        }
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(username = "test@gmx.at", authorities = {"PARENT"})
    public void testUnauthorizedLoadUser() {
        @SuppressWarnings("unused")
		User user = userService.loadUserByEmail("admin@gmx.at");
        Assert.fail("Call to userService.loadUser should not work without proper authorization for other users than the authenticated one");
    }

    @WithMockUser(username = "test@gmx.at", authorities = {"PARENT"})
    public void testAuthorizedLoadUser() {
        User user = userService.loadUser("3");
        Assert.assertEquals("Call to userService.loadUser returned wrong user", "3", user.getId());
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser
    public void testUnauthorizedSaveUser() {
        User user = userService.loadUser("2");
        Assert.assertEquals("Call to userService.loadUser returned wrong user", "2", user.getId());
        userService.saveUser(user);
    }

    @Test(expected = org.springframework.security.authentication.AuthenticationCredentialsNotFoundException.class)
    public void testUnauthenticatedDeleteUser() {
        User user = userService.loadUser("2");
        Assert.assertEquals("Call to userService.loadUser returned wrong user", "2", user.getId());
        userService.deleteUser(user);
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllEnabledParents() {
        Collection<User> users = userService.loadAllEnabledParents();

        Assert.assertNotNull(userService.loadUser("Parent_Id_1"));

        Assert.assertNotNull(users);
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_1")));
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_2")));
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_3")));
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_4")));
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_5")));
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_6")));
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_7")));
        Assert.assertTrue(users.contains(userService.loadUser("Parent_Id_8")));
        Assert.assertFalse(users.contains(userService.loadUser("Parent_Id_9")));
    }
    
    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllEmployees() {
        Collection<User> users = userService.loadAllEmployees();
        
        Assert.assertNotNull(users);
        Assert.assertTrue(users.contains(userService.loadUser("1")));
        Assert.assertTrue(users.contains(userService.loadUser("2")));
        Assert.assertFalse(users.contains(userService.loadUser("3")));
    }
    
    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllParents() {
        Collection<User> users = userService.loadAllParents();
        
        Assert.assertNotNull(users);
        Assert.assertFalse(users.contains(userService.loadUser("1")));
        Assert.assertFalse(users.contains(userService.loadUser("2")));
        Assert.assertTrue(users.contains(userService.loadUser("3")));
    }
    
    @DirtiesContext
    @Test
    @WithMockUser(username = "employee@gmx.at", authorities = {"EMPLOYEE"})
    public void testLoadAllAdmins() {
        Collection<User> users = userService.loadAllAdmins();
        
        Assert.assertNotNull(users);
        Assert.assertTrue(users.contains(userService.loadUser("1")));
        Assert.assertFalse(users.contains(userService.loadUser("2")));
        Assert.assertFalse(users.contains(userService.loadUser("3")));
    }
}
