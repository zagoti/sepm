INSERT INTO PERSON (DISCRIMINATOR, ENABLED, ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, CREATION_DATE) VALUES
('U' ,TRUE, '1', 'Admin', 'Istrator', 'admin@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2016-01-01 00:00:00'),
('U', TRUE, '2', 'Arbeit', 'er', 'employee@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2016-01-01 00:00:00'),
('U', TRUE, '3', 'Eltern', 'teil','test@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2016-01-01 00:00:00'),
('U', TRUE, 'Parent_Id_1', 'Parent_1_FN', 'Parent_1_LN','parent1@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', TRUE, 'Parent_Id_2', 'Parent_2_FN', 'Parent_2_LN','parent2@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', TRUE, 'Parent_Id_3', 'Parent_3_FN', 'Parent_3_LN','parent3@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', TRUE, 'Parent_Id_4', 'Parent_4_FN', 'Parent_4_LN','parent4@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', TRUE, 'Parent_Id_5', 'Parent_5_FN', 'Parent_5_LN','parent5@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', TRUE, 'Parent_Id_6', 'Parent_6_FN', 'Parent_6_LN','parent6@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', TRUE, 'Parent_Id_7', 'Parent_7_FN', 'Parent_7_LN','parent7@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', TRUE, 'Parent_Id_8', 'Parent_8_FN', 'Parent_8_LN','parent8@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00'),
('U', FALSE, 'Parent_Id_9', 'Parent_9_FN', 'Parent_9_LN','parent9@gmx.at', '$2a$10$n/r9HurPFd9JBE1B6cJIG.6UjfRKgjl23l8NI.Kn/1shetD1QaRJG', '2017-04-16 12:00:00');


INSERT INTO USER_USER_ROLE (USER_ID, ROLES) VALUES 
('1', 'ADMIN'),
('1', 'EMPLOYEE'),
('2', 'EMPLOYEE'),
('3', 'PARENT'),
('Parent_Id_1', 'PARENT'),
('Parent_Id_2', 'PARENT'),
('Parent_Id_3', 'PARENT'),
('Parent_Id_4', 'PARENT'),
('Parent_Id_5', 'PARENT'),
('Parent_Id_6', 'PARENT'),
('Parent_Id_7', 'PARENT'),
('Parent_Id_8', 'PARENT'),
('Parent_Id_9', 'PARENT');


INSERT INTO PERSON (DISCRIMINATOR, ID, FIRST_NAME, LAST_NAME, CREATION_DATE, CONFIRMED) VALUES
('C', '4', 'Care', 'Giver', '2016-01-01 00:00:00', TRUE),
('C', 'Caregiver_Id_1', 'Caregiver_1_FN', 'Caregiver_1_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_2', 'Caregiver_2_FN', 'Caregiver_2_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_3', 'Caregiver_3_FN', 'Caregiver_3_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_4', 'Caregiver_4_FN', 'Caregiver_4_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_5', 'Caregiver_5_FN', 'Caregiver_5_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_6', 'Caregiver_6_FN', 'Caregiver_6_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_7', 'Caregiver_7_FN', 'Caregiver_7_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_8', 'Caregiver_8_FN', 'Caregiver_8_LN', '2017-04-16 12:00:00', TRUE),
('C', 'Caregiver_Id_9', 'Caregiver_9_FN', 'Caregiver_9_LN', '2017-04-16 12:00:00', FALSE);


INSERT INTO CHILD (ENABLED, ID, FIRST_NAME, LAST_NAME, BIRTHDATE, CREATION_DATE,REMARKS, IMPORTANT_INFORMATION, REGISTRATION_DATE, EMERGENCY_CONTACT_ID, GENDER) VALUES
(TRUE, 'Child_Id_1', 'Child_1_FN', 'Child_1_LN', '2010-10-02', '2017-04-16 12:00:00', 'Test Remark 1', 'Test Important Info 1', '2017-04-01', 'Parent_Id_1', 'MALE'),
(TRUE, 'Child_Id_2', 'Child_2_FN', 'Child_2_LN', '2010-01-02', '2017-04-16 12:00:00', 'Test Remark 2', 'Test Important Info 2', '2017-04-01', 'Parent_Id_2', 'FEMALE'),
(TRUE, 'Child_Id_3', 'Child_3_FN', 'Child_3_LN', '2010-01-03', '2017-04-16 12:00:00', 'Test Remark 3', 'Test Important Info 3', '2017-04-01', 'Parent_Id_3', 'OTHER'),
(TRUE, 'Child_Id_4', 'Child_4_FN', 'Child_4_LN', '2010-01-04', '2017-04-16 12:00:00', 'Test Remark 4', 'Test Important Info 4', '2017-04-01', 'Parent_Id_4', 'MALE'),
(TRUE, 'Child_Id_5', 'Child_5_FN', 'Child_5_LN', '2010-01-05', '2017-04-16 12:00:00', 'Test Remark 5', 'Test Important Info 5', '2017-04-01', 'Parent_Id_5', 'FEMALE'),
(TRUE, 'Child_Id_6', 'Child_6_FN', 'Child_6_LN', '2010-01-06', '2017-04-16 12:00:00', 'Test Remark 6', 'Test Important Info 6', '2017-10-01', 'Parent_Id_6', 'OTHER'),
(TRUE, 'Child_Id_7', 'Child_7_FN', 'Child_7_LN', '2010-01-07', '2017-04-16 12:00:00', 'Test Remark 7', 'Test Important Info 7', '2017-10-01', 'Parent_Id_7', 'MALE'),
(TRUE, 'Child_Id_8', 'Child_8_FN', 'Child_8_LN', '2010-01-08', '2017-04-16 12:00:00', 'Test Remark 8', 'Test Important Info 8', '2017-10-01', 'Parent_Id_8', 'FEMALE');


INSERT INTO SIBLING (ID, BIRTHDATE, CREATION_DATE, FIRST_NAME, LAST_NAME) VALUES
('Sibling_Id_1', '2009-01-01', '2017-04-16 12:00:00', 'Sibling_1_FN', 'Sibling_1_LN'),
('Sibling_Id_2', '2009-01-02', '2017-04-16 12:00:00', 'Sibling_2_FN', 'Sibling_2_LN'),
('Sibling_Id_3', '2009-01-03', '2017-04-16 12:00:00', 'Sibling_3_FN', 'Sibling_3_LN'),
('Sibling_Id_4', '2009-01-04', '2017-04-16 12:00:00', 'Sibling_4_FN', 'Sibling_4_LN');



INSERT INTO PARENTS (PARENT_ID, CHILD_ID) VALUES
('Parent_Id_1', 'Child_Id_1'),
('Parent_Id_1', 'Child_Id_2'),
('Parent_Id_2', 'Child_Id_1'),
('Parent_Id_2', 'Child_Id_2'),
('Parent_Id_3', 'Child_Id_3'),
('Parent_Id_3', 'Child_Id_4'),
('Parent_Id_4', 'Child_Id_3'),
('Parent_Id_4', 'Child_Id_4'),
('Parent_Id_5', 'Child_Id_5'),
('Parent_Id_5', 'Child_Id_6'),
('Parent_Id_6', 'Child_Id_5'),
('Parent_Id_6', 'Child_Id_6'),
('Parent_Id_7', 'Child_Id_7'),
('Parent_Id_7', 'Child_Id_8'),
('Parent_Id_8', 'Child_Id_7'),
('Parent_Id_8', 'Child_Id_8');


INSERT INTO CHILDREN_CAREGIVER (CHILD_ID, CAREGIVER_ID) VALUES
('Child_Id_1', 'Caregiver_Id_1'),
('Child_Id_2', 'Caregiver_Id_2'),
('Child_Id_3', 'Caregiver_Id_3'),
('Child_Id_4', 'Caregiver_Id_4'),
('Child_Id_5', 'Caregiver_Id_5'),
('Child_Id_6', 'Caregiver_Id_6'),
('Child_Id_7', 'Caregiver_Id_7'),
('Child_Id_8', 'Caregiver_Id_8');


INSERT INTO CHILD_INTERNAL_SIBLINGS (CHILD_ID, SIBLING_ID) VALUES
('Child_Id_1', 'Child_Id_2'),
('Child_Id_2', 'Child_Id_1'),
('Child_Id_3', 'Child_Id_4'),
('Child_Id_4', 'Child_Id_3'),
('Child_Id_5', 'Child_Id_6'),
('Child_Id_6', 'Child_Id_5'),
('Child_Id_7', 'Child_Id_8'),
('Child_Id_8', 'Child_Id_7');


INSERT INTO CHILD_EXTERNAL_SIBLINGS (CHILD_ID, SIBLING_ID) VALUES
('Child_Id_1', 'Sibling_Id_1'),
('Child_Id_2', 'Sibling_Id_1'),
('Child_Id_3', 'Sibling_Id_2'),
('Child_Id_4', 'Sibling_Id_2'),
('Child_Id_5', 'Sibling_Id_3'),
('Child_Id_6', 'Sibling_Id_3'),
('Child_Id_7', 'Sibling_Id_4'),
('Child_Id_8', 'Sibling_Id_4');


INSERT INTO DAY_PLAN (DAY_DATE, CREATION_DATE, EARLIEST_ARRIVAL_DATE, LATEST_ARRIVAL_DATE, EARLIEST_DEPARTURE_DATE, LATEST_DEPARTURE_DATE, OPEN, MAX_ATTENDANCES) VALUES
('2017-10-01', '2017-04-20 12:00:00', '2017-10-01 8:00:00', '2017-10-01 9:45:00', '2017-10-01 22:00:00', '2017-10-01 23:00:00', TRUE, 20),
('2017-10-02', '2017-04-20 12:00:00', '2017-10-02 9:00:00', '2017-10-02 10:45:00', '2017-10-02 22:00:00', '2017-10-02 23:00:00', TRUE, 20),
('2017-10-03', '2017-04-20 12:00:00', '2017-10-03 7:00:00', '2017-10-03 8:15:00', '2017-10-03 22:00:00', '2017-10-03 22:03:00', TRUE, 20),
('2017-10-04', '2017-04-20 12:00:00', '2017-10-04 6:56:00', '2017-10-04 8:58:00', '2017-10-04 13:13:00', '2017-10-04 15:15:00', TRUE, 20);


INSERT INTO LUNCH (ID, CREATION_DATE, MENU, PRICE, DAYPLAN_ID) VALUES
('Lunch_Id_1', '2017-04-20 12:00:00', 'Tagesteller', 4.0, '2017-10-02'),
('Lunch_Id_2', '2017-04-20 12:00:00', 'Vegetarisch', 5.0, '2017-10-02'),
('Lunch_Id_3', '2017-04-20 12:00:00', 'Tagesteller', 4.0, '2017-10-03'),
('Lunch_Id_4', '2017-04-20 12:00:00', 'Vegetarisch', 5.0, '2017-10-03'),
('Lunch_Id_5', '2017-04-20 12:00:00', 'Tagesteller', 4.0, '2017-10-04'),
('Lunch_Id_6', '2017-04-20 12:00:00', 'Vegetarisch', 5.0, '2017-10-04');


INSERT INTO ATTENDANCE (ID, CREATION_DATE, DAY_PLAN_DATE, CHILD_ID, LUNCH_ID, PERSON_ID, ATTENDING) VALUES
('Attendance_Id_1', '2017-04-20 12:00:00', '2017-10-02', 'Child_Id_1', 'Lunch_Id_1', 'Parent_Id_1', 'true'),
('Attendance_Id_2', '2017-04-20 12:00:00', '2017-10-02', 'Child_Id_5', null , 'Parent_Id_5', 'true'),
('Attendance_Id_3', '2017-04-20 12:00:00', '2017-10-03', 'Child_Id_2', 'Lunch_Id_3', 'Parent_Id_1', 'true'),
('Attendance_Id_4', '2017-04-20 12:00:00', '2017-10-04', 'Child_Id_3', 'Lunch_Id_5', 'Parent_Id_4', 'true'),
('Attendance_Id_5', '2017-04-20 12:00:00', '2017-10-04', 'Child_Id_4', 'Lunch_Id_5', 'Parent_Id_4', 'true'),
('Attendance_Id_6', '2017-04-20 12:00:00', '2017-10-04', 'Child_Id_5', 'Lunch_Id_6', 'Parent_Id_5', 'true');


INSERT INTO MESSAGE (CREATION_DATE, SUBJECT, CONTENT, HIDE_MESSAGE, EMPLOYEE_MESSAGE, PUBLIC_MESSAGE, SENDER_ID, RECIPIENT_ID, CONFIRM_CAREGIVER_MESSAGE, ADDITIONAL_ID) VALUES
('2017-05-14 12:00:00', 'Employee Message Subject 1', 'Employee Message Content 1', FALSE, TRUE, FALSE, NULL, NULL, FALSE, NULL),
('2017-05-14 12:00:00', 'Employee Message Subject 2', 'Employee Message Content 2 - hidden', TRUE, TRUE, FALSE, NULL, NULL, FALSE, NULL),
('2017-05-14 12:00:00', 'Public Message Subject 1', 'Public Message Content 1', FALSE, FALSE, TRUE, NULL, NULL, FALSE, NULL),
('2017-05-14 12:00:00', 'Public Message Subject 2', 'Public Message Content 2 - hidden', TRUE, FALSE, TRUE, NULL, NULL, FALSE, NULL),
('2017-05-14 12:00:00', 'Personal Message Subject 1', 'Personal Message Content 1 sender:Parent_Id_1; recipient:Parent_Id_2', FALSE, FALSE, TRUE, 'Parent_Id_1', 'Parent_Id_2', FALSE, NULL),
('2017-05-14 12:00:00', 'Personal Message Subject 2', 'Personal Message Content 2 sender:Parent_Id_3; recipient:Parent_Id_4', FALSE, FALSE, TRUE, 'Parent_Id_3', 'Parent_Id_4', FALSE, NULL),
('2017-05-14 12:00:00', 'Confirm Caregiver Message Subject 1', 'Confirm Caregiver Message Content 1', FALSE, TRUE, FALSE, NULL, NULL, TRUE, 'Caregiver_Id_9');


INSERT INTO TASK (ID, CREATION_DATE, PARENT_ID, SUBJECT, DESCRIPTION, DEADLINE_FROM, DEADLINE_TO, INFO_DATE, COMPLETED) VALUES
('Task_Id_1', '2017-05-29 12:00:00', 'Parent_Id_1', 'A Subject', 'A Description', '2017-06-30', '2017-06-30', '2017-05-30', FALSE),
('Task_Id_2', '2017-05-29 12:00:00', 'Parent_Id_1', 'A Subject', 'A Description', '2017-06-30', '2017-07-04', '2017-05-30', TRUE),
('Task_Id_3', '2017-05-29 12:00:00', 'Parent_Id_1', 'Task 3 Subject', 'A Description', '2018-06-30', '2018-06-30', '2018-05-30', FALSE),
('Task_Id_4', '2017-05-29 12:00:00', 'Parent_Id_2', 'Task 4 Subject', 'Task 4 Description', '2017-06-30', '2017-07-04', '2017-05-30', FALSE);
