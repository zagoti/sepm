package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.Message;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;

/**
 * Service for accessing and manipulating {@link Message} data.
 * 
 */
@Component
@Scope("application")
public class MessageService {

	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private UserService userService;

	/**
	 * Loads a single message identified by its id.
	 *
	 * @param id
	 *            the id to search for
	 * @return the message with the given id
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Message loadMessage(Long id) {
		return messageRepository.findFirstById(id);
	}

	/**
	 * Loads a collection of all messages.
	 *
	 * @return collection of all messages
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE')")
	public Collection<Message> loadAllMessages() {
		return messageRepository.findAll();
	}

	/**
	 * Loads a collection of all messages for employees.
	 *
	 * @return collection of all messages for employees
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE')")
	public Collection<Message> loadAllEmployeeMessages() {
		return messageRepository.findAllEmployeeMessages();
	}

	/**
	 * Loads a collection of all visible messages for employees.
	 *
	 * @return collection of all visible messages for employees
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE')")
	public Collection<Message> loadAllVisibleEmployeeMessages() {
		return messageRepository.findAllVisibleEmployeeMessages();
	}

	/**
	 * Loads a collection of all public messages.
	 *
	 * @return collection of all public messages
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE')")
	public Collection<Message> loadAllPublicMessages() {
		return messageRepository.findAllPublicMessages();
	}

	/**
	 * Loads a collection of all visible public messages.
	 *
	 * @return collection of all visible public messages
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('PARENT')")
	public Collection<Message> loadAllVisiblePublicMessages() {
		return messageRepository.findAllVisiblePublicMessages();
	}

	/**
	 * Loads a collection of all messages that contain the given string in the
	 * subject.
	 *
	 * @param string
	 *            the string to search for
	 * @return collection of all messages containing string in subject
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE')")
	public Collection<Message> loadAllMessagesSubjectContains(String string) {
		return messageRepository.findSubjectContains(string);
	}

	/**
	 * Saves the message. This method will also set {@link Message#creationDate}
	 * for new entities.
	 *
	 * @param message
	 *            the message to save
	 * @return the updated message
	 */
	public Message saveMessage(Message message) {
		if (message.isNew())
			message.setCreationDate(Calendar.getInstance());
		return messageRepository.save(message);
	}

	/**
	 * Deletes the message.
	 *
	 * @param message
	 *            the message to delete
	 */
	@PreAuthorize("hasAuthority('ADMIN')")
	public void deleteMessage(Message message) {
		messageRepository.delete(message);
	}

	/**
	 * Creates a employee message and saves it in the DB.
	 *
	 * @param subject
	 *            subject of the message
	 * @param content
	 *            content of the message
	 * @return the saved message
	 */
	public Message createEmployeeMessage(String subject, String content) {
		Message message = new Message(subject, content, false, true, false);
		return saveMessage(message);
	}

	/**
	 * Creates a employee message for confirming a caregiver and saves it in the
	 * DB.
	 *
	 * @param subject
	 *            subject of the message
	 * @param content
	 *            content of the message
	 * @return the saved message
	 */
	public Message createCaregiverConfirmationMessage(String subject, String content, String caregiverId) {
		Message message = new Message(subject, content, false, true, false);
		message.setConfirmCaregiverMessage(true);
		message.setAdditionalId(caregiverId);
		return saveMessage(message);
	}

	/**
	 * Creates a public message and saves it in the DB.
	 *
	 * @param subject
	 *            subject of the message
	 * @param content
	 *            content of the message
	 * @return the saved message
	 */
	public Message createPublicMessage(String subject, String content) {
		Message message = new Message(subject, content, false, false, true);
		return saveMessage(message);
	}

	/**
	 * Creates a user message with the authenticated user as sender and saves it
	 * in the DB.
	 *
	 * @param subject
	 *            subject of the message
	 * @param content
	 *            content of the message
	 * @param recipient
	 *            recipient of the message
	 * @return the saved message
	 */
	public Message createUserMessage(String subject, String content, User recipient) {
		User sender = getAuthenticatedUser();
		Message message = new Message(subject, content, false, false, false, sender, recipient);
		return saveMessage(message);
	}

	/**
	 * Helper method to get current user
	 *
	 */
	private User getAuthenticatedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth == null) {
			return null;
		} else {
			return userService.loadUserByEmail(auth.getName());
		}
	}
}
