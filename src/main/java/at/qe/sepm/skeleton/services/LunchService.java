package at.qe.sepm.skeleton.services;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.repositories.LunchRepository;

/**
 * Service for accessing and manipulating {@link Lunch} data.
 * 
 */
@Component
@Scope("application")
public class LunchService {

	@Autowired
	private LunchRepository lunchRepository;

	/**
	 * Loads a single lunch identified by its date.
	 *
	 * @param id
	 *            the lunch id to search for
	 * @return the lunch with the given id
	 */
	public Lunch loadLunch(String id) {
		return lunchRepository.findFirstById(id);
	}

	/**
	 * Loads a list of lunches
	 * 
	 * @param consumer
	 * @return
	 */
	public List<Lunch> loadLunchesByConsumer(Attendance consumer) {
		return lunchRepository.findByConsumer(consumer);
	}

	/**
	 * Saves the Lunch. This method will also set {@link Lunch#creationDate} for
	 * new entities.
	 * 
	 * @param lunch
	 *            the lunch to save
	 * @return the updated lunch
	 */
	public Lunch saveLunch(Lunch lunch) {
		if (lunch.isNew()) {
			lunch.setCreationDate(Calendar.getInstance());
			// TODO: logging
		} else {
			// TODO: logging
		}
		return lunchRepository.save(lunch);
	}

	public void deleteLunch(Lunch lunch) {
		lunchRepository.delete(lunch);
	}
}
