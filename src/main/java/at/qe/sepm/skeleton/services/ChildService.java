package at.qe.sepm.skeleton.services;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.repositories.ChildRepository;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Service for accessing and manipulating {@link Child} data.
 * 
 */
@Component
@Scope("application")
public class ChildService {

	@Autowired
	private ChildRepository childRepository;

	@Autowired
	private SiblingService siblingService;

	@Autowired
	private UserService userService;

	@Autowired
	private AttendanceService attendanceService;

	@Autowired
	private CaregiverService caregiverService;

	/**
	 * Returns a collection of all children.
	 *
	 * @return a collection of all children.
	 */
	public Collection<Child> loadAllChildren() {
		return childRepository.findAll();
	}

	/**
	 * Loads a single child identified by its id.
	 *
	 * @param id
	 *            the id to search for
	 * @return the child with the given id
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child loadChild(String id) {
		User currentUser = getAuthenticatedUser();
		Child child = childRepository.findFirstById(id);
		if (isEmployeeOrAdmin(currentUser) || currentUser.getChildren().contains(child)) {
			return childRepository.findFirstById(id);
		} else
			throw new AccessDeniedException("The authenticated User " + currentUser.getId()
					+ " doesn't have Permission to load child " + child.getId());
	}
	
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child loadChildWhileForcingAttendances(String id) {
		Child child = childRepository.findFirstByIdAndFetchAttendancesEagerly(id);
		return child;
	}

	/**
	 * Loads a list of children identified by their whole name.
	 *
	 * @param name
	 *            the name to search for
	 * @return list of children with the given name
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public List<Child> loadChildByName(String name) {
		return childRepository.findByWholeNameConcat(name);
	}

	/**
	 * Loads a list of all children that match the given status.
	 * 
	 * @param enabled
	 *            the status
	 * @return a list of children with the status
	 */
	public List<Child> loadChildrenByEnabled(boolean enabled) {
		return childRepository.findByEnabled(enabled);
	}

	/**
	 * Loads a list of children identified by one of their parents.
	 * 
	 * @param parent
	 *            the parent of the children to search for
	 * @return list of children with the given parent
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public List<Child> loadChildrenByParent(@Param("parent") User parent) {
		User currentUser = getAuthenticatedUser();
		if (isEmployeeOrAdmin(currentUser) || currentUser.equals(parent)) {
			return childRepository.findByParent(userService.loadUser(parent.getId()));
		} else
			throw new AccessDeniedException("The authenticated User " + currentUser.getId()
					+ " doesn't have Permission to load the children of " + parent.getId());
	}

	/**
	 * Saves the child. This method will also set {@link Child#creationDate} for
	 * new entities.
	 *
	 * @param child
	 *            the child to save
	 * @return the updated child
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child saveChild(Child child) {
		User currentUser = getAuthenticatedUser();
		if (isEmployeeOrAdmin(currentUser) || currentUser.getChildren().contains(child)) {
			if (child.isNew())
				child.setCreationDate(Calendar.getInstance());
			return childRepository.save(child);
		} else
			throw new AccessDeniedException("The authenticated User " + currentUser.getId()
					+ " doesn't have Permission to save child " + child.getId());
	}

	/**
	 * Creates a new Child and saves it in the Database.
	 *
	 * @param firstName
	 * @param lastName
	 * @param birthdate
	 * @param gender
	 * @param registrationDate
	 *
	 * @return the newly created child
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child createChild(String firstName, String lastName, Gender gender, Calendar birthdate,
			Calendar registrationDate) {
		Child child = new Child(firstName, lastName, gender, birthdate, registrationDate);
		child.getAttendances();
		child.setEnabled(true);
		return saveChild(child);
	}

	/**
	 * Deletes the child.
	 *
	 * @param child
	 *            the child to delete
	 */
	@PreAuthorize("hasAuthority('ADMIN')")
	public void deleteChild(Child child) {

		child = childRepository.findFirstByIdAndFetchAttendancesEagerly(child.getId());

		for (Child relatedChild : child.getInternalSiblings()) {
			relatedChild.removeInternalSibling(child);
			saveChild(relatedChild);
		}

		for (Sibling relatedSibling : child.getExternalSiblings()) {
			relatedSibling.removeSiblingOf(child);
			siblingService.saveSibling(relatedSibling);
		}

		for (User parent : child.getParents()) {
			parent.removeChild(child);
			userService.saveUser(parent);
		}

		for (Caregiver caregiver : child.getCaregivers()) {
			child.removeCaregiver(caregiver);
			child = saveChild(child);
		}

		for (Attendance attendance : child.getAttendances()) {
			child.removeAttendance(attendance);
			child = childRepository.save(child);
			attendanceService.deleteAttendance(attendance);
		}

		childRepository.delete(child);
		// :TODO: write some audit log stating who and when this user was
		// permanently deleted.
	}

	/**
	 * Adds a internal sibling relation to both children and all siblings of
	 * those children
	 *
	 * @param sibling1
	 * @param sibling2
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child addInternalSiblingRelation(Child sibling1, Child sibling2) {
		User currentUser = getAuthenticatedUser();
		if (!(isParentEmployeeOrAdmin(currentUser, sibling1) && isParentEmployeeOrAdmin(currentUser, sibling2)))
			throw new AccessDeniedException(
					"The authenticated User " + currentUser.getId() + " doesn't have Permission to add a Sibling");
		Set<Child> siblings = new HashSet<>();
		siblings.add(sibling1);
		siblings.add(sibling2);
		siblings.addAll(sibling1.getInternalSiblings());
		siblings.addAll(sibling2.getInternalSiblings());

		for (Child c : siblings) {
			c.addAllInternalSibling(siblings);
			c.removeInternalSibling(c);
			saveChild(c);
		}
		return loadChild(sibling1.getId());
	}

	/**
	 * Remove a child as an internal sibling from a child and all it's other
	 * siblings
	 *
	 * @param child
	 * @param siblingToRemove
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child removeInternalSiblingRelation(Child child, Child siblingToRemove) {
		User currentUser = getAuthenticatedUser();
		if (!(isParentEmployeeOrAdmin(currentUser, child) && isParentEmployeeOrAdmin(currentUser, siblingToRemove)))
			throw new AccessDeniedException(
					"The authenticated User " + currentUser.getId() + " doesn't have Permission to add a Sibling");

		Set<Child> siblings = new HashSet<>();
		siblings.add(child);
		siblings.addAll(child.getInternalSiblings());
		siblings.addAll(siblingToRemove.getInternalSiblings());
		siblings.remove(siblingToRemove);

		for (Child c : siblings) {
			c.removeInternalSibling(siblingToRemove);
			siblingToRemove.removeInternalSibling(c);
			saveChild(c);
			saveChild(siblingToRemove);
		}
		return loadChild(child.getId());
	}

	/**
	 * Adds a external sibling relation between the child all internal siblings
	 * of the child and the given external sibling
	 *
	 * @param child
	 * @param sibling
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child addExternalSiblingRelation(Child child, Sibling sibling) {
		User currentUser = getAuthenticatedUser();
		if (!(isParentEmployeeOrAdmin(currentUser, child)))
			throw new AccessDeniedException(
					"The authenticated User " + currentUser.getId() + " doesn't have Permission to add a Sibling");
		Set<Child> siblings = new HashSet<>();
		siblings.add(child);
		siblings.addAll(child.getInternalSiblings());

		for (Child c : siblings) {
			c.addExternalSibling(sibling);
			sibling.addSiblingOf(c);
			saveChild(c);
		}
		siblingService.saveSibling(sibling);
		return loadChild(child.getId());
	}

	/**
	 * Adds a external sibling relation between the child all internal siblings
	 * of the child and the given external sibling
	 *
	 * @param child
	 * @param siblings
	 *            Set of external siblings
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child addAllExternalSiblingRelations(Child child, Set<Sibling> siblings) {
		for (Sibling s : siblings)
			addExternalSiblingRelation(loadChild(child.getId()), s);
		return loadChild(child.getId());
	}

	/**
	 * Remove a child as an internal sibling from a child and all it's other
	 * siblings
	 *
	 * @param child
	 * @param siblingToRemove
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child removeExternalSiblingRelation(Child child, Sibling siblingToRemove) {
		User currentUser = getAuthenticatedUser();
		if (!(isParentEmployeeOrAdmin(currentUser, child)))
			throw new AccessDeniedException(
					"The authenticated User " + currentUser.getId() + " doesn't have Permission to add a Sibling");

		Set<Child> siblings = new HashSet<>();
		siblings.add(child);
		siblings.addAll(child.getInternalSiblings());
		siblings.addAll(siblingToRemove.getSiblingOf());

		for (Child c : siblings) {
			c.removeExternalSibling(siblingToRemove);
			siblingToRemove.removeSiblingOf(c);
			saveChild(c);
		}
		siblingService.saveSibling(siblingToRemove);
		return loadChild(child.getId());
	}

	/**
	 * Adds a child - parent relation to the child and the parent
	 *
	 * @param child
	 * @param parent
	 *
	 * @return the updated child
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child addParent(Child child, User parent) {
		child.addParent(parent);
		parent.addChild(child);
		userService.saveUser(parent);
		return saveChild(child);
	}

	/**
	 * Removes a child - parent relation
	 *
	 * @param child
	 * @param parent
	 *
	 * @return the updated child
	 */
	@PreAuthorize("hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child removeParent(Child child, User parent) {
		child.removeParent(parent);
		parent.removeChild(child);
		userService.saveUser(parent);
		return saveChild(child);
	}

	/**
	 * Adds a child - caregiver relation to the child and the parent
	 *
	 * @param child
	 * @param caregiver
	 *
	 * @return the updated child
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child addCaregiver(Child child, Caregiver caregiver) {
		User currentUser = getAuthenticatedUser();
		if (!(isParentEmployeeOrAdmin(currentUser, child)))
			throw new AccessDeniedException("The authenticated User " + currentUser.getId()
					+ " doesn't have Permission to add a Caregiver to child " + child.getId());

		child.addCaregiver(caregiver);
		caregiver.addChild(child);
		caregiverService.saveCaregiver(caregiver);
		return saveChild(child);
	}

	/**
	 * Remove a child - caregiver relation
	 *
	 * @param child
	 * @param caregiver
	 *
	 * @return the updated child
	 */
	@PreAuthorize("hasAuthority('PARENT') or hasAuthority('EMPLOYEE') or hasAuthority('ADMIN')")
	public Child removeCaregiver(Child child, Caregiver caregiver) {
		User currentUser = getAuthenticatedUser();
		if (!(isParentEmployeeOrAdmin(currentUser, child)))
			throw new AccessDeniedException("The authenticated User " + currentUser.getId()
					+ " doesn't have Permission to remove a Caregiver from child " + child.getId());

		child.removeCaregiver(caregiver);
		caregiver.removeChild(child);
		caregiverService.saveCaregiver(caregiver);
		return saveChild(child);
	}

	/**
	 * Helper method to determine if a user is a parent of a child or an
	 * employee/admin
	 * 
	 * @param child
	 *            the child to check for parents
	 * @param currentUser
	 *            the user which is to be checked
	 */
	private boolean isParentEmployeeOrAdmin(User currentUser, Child child) {
		if (isEmployeeOrAdmin(currentUser) || currentUser.getChildren().contains(child))
			return true;
		return false;
	}

	/**
	 * Helper method to determine if a user is an employee/admin
	 *
	 * @param user
	 *            the user which is to be checked
	 */
	private boolean isEmployeeOrAdmin(User user) {
		return user.getRoles().contains(UserRole.EMPLOYEE) || user.getRoles().contains(UserRole.ADMIN);
	}

	/**
	 * Helper method to get current user
	 * 
	 */
	private User getAuthenticatedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null) {
			return null;
		} else {
			return userService.loadUserByEmail(auth.getName());
		}
	}
}
