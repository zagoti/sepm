package at.qe.sepm.skeleton.repositories;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.Task;
import at.qe.sepm.skeleton.model.UserRole;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for managing {@link User} entities.
 * 
 */
public interface UserRepository extends AbstractRepository<User, String> {

	User findFirstById(String id);

	User findFirstByEmail(String email);

	List<User> findByPrivatePhone(String phoneNumber);

	List<User> findByBusinessPhone(String phoneNumber);

	@Query("SELECT tasks FROM User u WHERE u.id = :id")
	List<Task> findTasksById(@Param("id") String id);

	@Query("SELECT children FROM User u WHERE u.id = :id")
	List<Child> findChildrenById(@Param("id") String id);

	@Query("SELECT u FROM User u WHERE CONCAT(u.firstName, ' ', u.lastName) = :wholeName")
	List<User> findByWholeNameConcat(@Param("wholeName") String wholeName);

	@Query("SELECT u FROM User u WHERE :role MEMBER OF u.roles")
	List<User> findByRole(@Param("role") UserRole role);

	@Query("SELECT u FROM User u WHERE :role MEMBER OF u.roles AND u.enabled is TRUE")
	List<User> findEnabledByRole(@Param("role") UserRole role);

}
