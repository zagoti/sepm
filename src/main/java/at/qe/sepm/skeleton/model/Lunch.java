package at.qe.sepm.skeleton.model;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing lunches of certain days.
 * 
 */
@Entity
public class Lunch implements Persistable<String> {

	private static final long serialVersionUID = 1L;

	@Id
	private final String id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	private String menu;
	private float price;

	@ManyToOne(optional = false)
	@JoinColumn(name = "DAYPLAN_ID")
	private DayPlan dayPlan;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "lunch")
	private Set<Attendance> consumers;

	public Lunch() {
		id = UUID.randomUUID().toString();
		consumers = new HashSet<>();
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public boolean isNew() {
		return creationDate == null;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public DayPlan getDayPlan() {
		return dayPlan;
	}

	public void setDayPlan(DayPlan dayPlan) {
		this.dayPlan = dayPlan;
	}

	public Set<Attendance> getConsumers() {
		return consumers;
	}

	public void setConsumers(Set<Attendance> consumers) {
		this.consumers = consumers;
	}

	public void addAttendance(Attendance consumer) {
		consumers.add(consumer);
	}

	public void removeConsumer(Attendance consumer) {
		consumers.remove(consumer);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.getId());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Lunch)) {
			return false;
		}
		final Lunch other = (Lunch) obj;
		if (!Objects.equals(this.getId(), other.getId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "at.qe.sepm.skeleton.model.Lunch[ id='" + this.getId() + "']";
	}

}
