package at.qe.sepm.skeleton.model;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing plans of certain days.
 * 
 */
@Entity
public class DayPlan implements Persistable<Calendar> {

	private static final long serialVersionUID = 1L;

	@Id
	@Temporal(TemporalType.DATE)
	private Calendar dayDate;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar creationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar earliestArrivalDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar latestArrivalDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar earliestDepartureDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar latestDepartureDate;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "dayPlan")
	private Set<Attendance> attendances;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "dayPlan")
	private Set<Lunch> lunches;

	private boolean open;
	private int maxAttendances;
	
	public DayPlan() {
		attendances = new HashSet<>();
		lunches = new HashSet<>();
	}

	@Override
	public Calendar getId() {
		return dayDate;
	}

	@Override
	public boolean isNew() {
		return creationDate == null;
	}

	public Calendar getDayDate() {
		return dayDate;
	}

	public void setDayDate(Calendar dayDate) {
		this.dayDate = dayDate;
	}

	public Calendar getEarliestArrivalDate() {
		return earliestArrivalDate;
	}

	public void setEarliestArrivalDate(Calendar earliestArrivalDate) {
		this.earliestArrivalDate = earliestArrivalDate;
	}

	public Calendar getLatestArrivalDate() {
		return latestArrivalDate;
	}

	public void setLatestArrivalDate(Calendar latestArrivalDate) {
		this.latestArrivalDate = latestArrivalDate;
	}

	public Calendar getEarliestDepartureDate() {
		return earliestDepartureDate;
	}

	public void setEarliestDepartureDate(Calendar earliestDepartureDate) {
		this.earliestDepartureDate = earliestDepartureDate;
	}

	public Calendar getLatestDepartureDate() {
		return latestDepartureDate;
	}

	public void setLatestDepartureDate(Calendar latestDepartureDate) {
		this.latestDepartureDate = latestDepartureDate;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Set<Attendance> getAttendances() {
		return attendances;
	}

	public void setAttendances(Set<Attendance> attendances) {
		this.attendances = attendances;
	}

	public void addAttendance(Attendance attendance) {
		attendances.add(attendance);
	}

	public void removeAttendance(Attendance attendance) {
		attendances.remove(attendance);
	}

	public Set<Lunch> getLunches() {
		return lunches;
	}

	public void setLunches(Set<Lunch> lunches) {
		this.lunches = lunches;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public int getMaxAttendances() {
		return maxAttendances;
	}

	public void setMaxAttendances(int maxAttendances) {
		this.maxAttendances = maxAttendances;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 59 * hash + Objects.hashCode(this.getId());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DayPlan)) {
			return false;
		}
		final DayPlan other = (DayPlan) obj;
		if (!Objects.equals(this.getId(), other.getId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "at.qe.sepm.skeleton.model.DayPlan[ id='" + this.getId() + "']";
	}

}
