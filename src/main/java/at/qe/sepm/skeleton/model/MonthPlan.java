package at.qe.sepm.skeleton.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.domain.Persistable;

/**
 * Model format for data of a certain month.
 *
 */
@Entity
public class MonthPlan implements Persistable<String> {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    public int monthId;
    public String month;
    public int ovrOccupancy;
    public int avrOccupancy;
    public int open;

    public MonthPlan() {
    }
    
    public MonthPlan(int monthId, String month, int ovrOccupancy, int avrOccupancy, int open) {
        this.monthId = monthId;
        this.month = month;
        this.ovrOccupancy = ovrOccupancy;
        this.avrOccupancy = avrOccupancy;
        this.open = open;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getOvrOccupancy() {
        return ovrOccupancy;
    }

    public void setOvrOccupancy(int ovrOccupancy) {
        this.ovrOccupancy = ovrOccupancy;
    }

    public int getAvrOccupancy() {
        return avrOccupancy;
    }

    public void setAvrOccupancy(int avrOccupancy) {
        this.avrOccupancy = avrOccupancy;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public boolean isNew() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public int getMonthId() {
        return monthId;
    }
    
    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

}
