package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Controller for the caregiver creation.
 *
 */
@Component
@Scope("view")
public class CaregiverCreateController {

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private ChildService childService;

    @Autowired
    private SessionInfoBean sessionInfoBean;

    private Caregiver caregiver;

    private List<String> selectedChildren;

    public List<String> getSelectedChildren() {
        return this.selectedChildren;
    }

    /**
     * Method to set which children are set.
     *
     * @param children
     */
    public void setSelectedChildren(List<String> children) {
        if (children.size() != 0) {
            this.selectedChildren = children;
        }
    }

    /**
     * Get current Caregiver object from controller.
     *
     * @return Caregiver object
     */
    public Caregiver getCaregiver() {
        return caregiver;
    }

    /**
     * Sets the Caregiver object of the controller
     *
     * @param caregiver
     *            value to set controller's caregiver to
     */
    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    /**
     * Initialize with a new Caregiver and empty SelectedChildren
     */
    @PostConstruct
    public void init() {
        caregiver = new Caregiver();
        selectedChildren = new LinkedList<>();

        // set all possible children as standart selection
        for (Child c : getPossibleChildren())
            selectedChildren.add(c.getId());
    }

    /**
     * Return all children of current logged-in user.
     *
     * @return Collection of children
     */
    public Collection<Child> getPossibleChildren() {
        return childService.loadChildrenByParent(sessionInfoBean.getCurrentUser());
    }

    /**
     * Reload Caregiver object from database.
     *
     */
    public void reload() {
        if (caregiver == null) return;
        caregiver = caregiverService.loadCaregiver(caregiver.getId());
    }

    /**
     * Save Caregiver object in the database.
     *
     */
    public void save() {
        System.out.println("Caregiver: " + caregiver);
        //add children
        if (selectedChildren != null && selectedChildren.size() != 0) {
            for (String child : selectedChildren) {
                Child childObject = childService.loadChild(child);
                //save child in caregiver
                caregiver.addChild(childObject);
                caregiver = caregiverService.saveCaregiver(caregiver);
                //save caregiver in child
                childObject.addCaregiver(caregiver);
                childService.saveChild(childObject);
            }
            // save caregiver in db so he can be referenced
            caregiverService.saveCaregiver(caregiver);
        }

        // reset caregiver
        caregiver = new Caregiver();

        // reset selection to default
        for (Child c : getPossibleChildren())
            selectedChildren.add(c.getId());
    }
}

