package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Caregiver;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;

/**
 * Message schedule bean to send automatically sent messages.
 *
 * This class checks daily at 01:01am if new messages has to
 * be sent.
 * A new message is generated when a childs deregistration date is
 * exceeded, but is still enabled, a child has birthday today or a
 * caregiver is still unconfirmed.
 */
@Component
@DependsOn("dayPlanServiceConfig")
@Scope("singleton")
public class MessageScheduleBean {

    @Autowired
    private MessageService messageService;

    @Autowired
    private DayPlanService dayPlanService;

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private ChildService childService;

    /**
     * Simple cron job method which executes every day at 01:01 AM
     *
     */
    @Scheduled(cron="0 1 1 * * ?")
    public void dailyScheduledMessages() {
        Calendar today = Calendar.getInstance();
        checkForDeregisteredChildren(today);
        checkForBirthdays(today);
        checkForUnconfirmedCaregivers();
    }

    /**
     * Checks if there are children who should already be unregistered
     * but are still enabled and sends a reminder
     * employee message for each of the children.
     *
     * @param today
     *            the date to check for
     */
    public void checkForDeregisteredChildren(Calendar today) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

        for (Child c : childService.loadAllChildren()) {
            if (c != null && c.isEnabled() && c.getDeregistrationDate() != null && c.getDeregistrationDate().before(today)) {
                String formattedDeregistrationDate = format.format(c.getDeregistrationDate().getTime());
                messageService.createEmployeeMessage("Überschrittenes Abmeldedatum", c.getFirstName()
                        + " " + c.getLastName() + " hätte mit dem " + formattedDeregistrationDate + " abgemeldet werden sollen.");
            }
        }
    }

    /**
     * Checks if children have birthday on a given date and
     * sends a reminder employee message for each of the
     * children who have birthday.
     *
     * @param today
     *            the date to check for
     */
    public void checkForBirthdays(Calendar today) {
        DayPlan dayPlan = dayPlanService.loadDayPlan(today);
        today = removeTimeFromDate(today);

        if (dayPlan == null)
            return;

        Set<Attendance> attendanceSet = dayPlan.getAttendances();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

        for (Attendance a : attendanceSet) {
            Child child = a.getChild();
            if (child != null && child.getBirthdate().get(Calendar.MONTH) == today.get(Calendar.MONTH)
                    && child.getBirthdate().get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
                messageService.createEmployeeMessage("Geburtstag", child.getFirstName() + " "
                        + child.getLastName() + " hat heute Geburtstag.");
            }
        }
    }

    /**
     * Checks for unconfirmed caregivers and sends a reminder
     * employee message for each of them.
     *
     */
    public void checkForUnconfirmedCaregivers() {
        for (Caregiver caregiver : caregiverService.loadAllUnconfirmedCaregivers()) {
            messageService.createCaregiverConfirmationMessage("Nicht bestätigte Bezugsperson", caregiver.getFirstName()
                    + " " + caregiver.getLastName() + " wurde noch nicht als Bezugsperson bestätigt.", caregiver.getId());
        }
    }

    /**
     * Simple helper method to set the time components
     * of an calender to 0.
     *
     * @param date
     * @return date with time components set to 0
     */
    private Calendar removeTimeFromDate(Calendar date) {
        Calendar dateWithoutTime = date;
        dateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
        dateWithoutTime.set(Calendar.MINUTE, 0);
        dateWithoutTime.set(Calendar.SECOND, 0);
        dateWithoutTime.set(Calendar.MILLISECOND, 0);
        return dateWithoutTime;
    }
}
