package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.DayPlan;
import at.qe.sepm.skeleton.model.Lunch;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.DayPlanService;
import at.qe.sepm.skeleton.services.LunchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.*;

//TODO: Docu for Class
@Component
@Scope("session")
public class LunchConfigurationBean {

    @Autowired
    DayPlanService dayPlanService;
    
    @Autowired
    LunchService lunchService;
    
    @Autowired
    ChildService childService;
    
    @Autowired
    SessionInfoBean sessionInfoBean;

    private Calendar savedWeek;
    
    private Calendar savedDay;
    
    private String lunch;
    
    private List<Child> children;

    /**
     * Initialize the current week
     * Set the current week to the actual current week
     *
     */
    @PostConstruct
    public void init() {
        Calendar currentWeek = Calendar.getInstance();
        currentWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        this.savedWeek = currentWeek;
    }

    /**
     * Get the current week as String in the form 'dd.mm.yyyy - dd.mm.yyyy' 
     * 
     * @return week range as String
     */
    public String getWeekRange() {
        return ("Mo: " + getDayOfWeekAsString(1) + " - So: " + getDayOfWeekAsString(7));
    }

    /**
     * Get a specific day in the current week with the attribute indicating,
     * which day of the week you want
     * 
     * @param index
     *            as int between 1 and 7
     * @return day of the week as Calendar
     */
    public Calendar getDayOfWeek(int index) {
        index -= 1;
        if(index < 0 || index > 7) return null;
        
        Calendar saved = Calendar.getInstance();
        saved.setTime(savedWeek.getTime());
        
        if (index != 0) {
            saved.add(Calendar.DAY_OF_WEEK, index);
        }
        return saved;
    }

    /**
     * Get the day of the week with the attribute indicating, the day 
     * in the format 'dd.mm.yyyy'
     * 
     * @param index
     *            as int
     * @return day of the week as String
     */
    public String getDayOfWeekAsString(int index) {
        SimpleDateFormat converter = new SimpleDateFormat("dd.MM.yyyy");
        return converter.format(getDayOfWeek(index).getTime());
    }

    /**
     * Get Calendar date as String in format 'dd.mm.yyyy'
     * 
     * @param date
     *            as Calendar
     * @return date as String
     */
    public String convertToString(Calendar date) {
        if (date == null) return "not set";
        SimpleDateFormat converter = new SimpleDateFormat("dd.MM.yyyy");
        return converter.format(date.getTime());
    }

    /**
     * Set current week to the following week
     *
     */
    public void setNextWeek() {
        savedWeek.add(Calendar.DAY_OF_MONTH, 7);

    }

    /**
     * Set current week to the previous week
     *
     */
    public void setPrevWeek() {
        savedWeek.add(Calendar.DAY_OF_MONTH, -7);
    }

    /**
     * Set current week to actual current week
     *
     */
    public void setToday() {
        init();
    }
        
    /**
     * Get all lunches for the given day
     * 
     * @param day
     *            as DayPlan
     * @return all lunches as Collection of lunches
     */
    public Collection<Lunch> getLunches(DayPlan day) {
        return day.getLunches();
    }

    /**
     * Get all children related to the current user
     * 
     * @return children as Collection of children
     */
    public Collection<Child> getPossibleChildren() {
        return childService.loadChildrenByParent(sessionInfoBean.getCurrentUser());
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    /**
     * Get current day
     *
     * @return day as Calendar
     */
    public Calendar getSavedDay() {
        return savedDay;
    }

    /**
     * Set saved day with index, indicating the day of the actual current week
     *
     * @param index
     *            as int
     */
    public void setSavedDay(int index) {
        Calendar gotDay = Calendar.getInstance();
        gotDay.setTime(getDayOfWeek(index).getTime());
        this.savedDay = gotDay;
    }

    /** 
     * Get saved day in form 'dd.mm.yyyy'
     * 
     * @return savedDay as String
     */
    public String getSavedDayAsString() {
        SimpleDateFormat converter = new SimpleDateFormat("dd.MM.yyyy");
        if (savedDay == null) return "No day has been selected yet";
        return converter.format(savedDay.getTime());
    }

    /**
     * Get all children
     *
     * @return children as Collection of children
     */
    public Collection<Child> getChildren() {
        return children;
    }

    /**
     * Set current children 
     * 
     * @param children
     *            as String[] of children
     */
    public void setChildren(String[] children) {
        for (Child c : childService.loadChildrenByParent(sessionInfoBean.getCurrentUser())) {
            if (Arrays.toString(children).contains(c.toString())) {
                this.children.add(c);
            }
        }
    }

    /**
     * Get name of given day in german
     * 
     * @param day
     *            as DayPlan
     * @return name of weekday
     */
    public String getWeekday(DayPlan day) {
        return day.getDayDate().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.GERMAN);
    }
}
