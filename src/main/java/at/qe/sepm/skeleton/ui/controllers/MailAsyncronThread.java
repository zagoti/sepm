package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.services.MailService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

/**
 * Class created specific for the MailController to send mails via a multithreading.
 * This is necessary, because sending mail requires quit some time and timeouts are 
 * a huge problem. To make the GUI usable while sending, the process has to be done
 * in the background.
 *
 * @author robert
 */
public class MailAsyncronThread implements Runnable{
    
    private final MailService mailService;
    private String to;
    private String[] multipleTo;
    private final String subject;
    private final String message;   
    private final boolean multiple;
    
    /**
     * Constructor to set all attributes
     *
     * @param mailService
     * @param to
     *            (Receiver of the mail)
     * @param subject
     * @param message 
     */
    public MailAsyncronThread(MailService mailService, String to, String subject, String message){
        this.mailService = mailService;
        this.to = to;
        this.subject = subject;
        this.message = message;
        this.multiple = false;
        
    }
    
    /**
     * Constructor to set all attributes
     *
     * @param mailService
     * @param multipleTo
     *            (Receiver of the mail)
     * @param subject
     * @param message 
     */
    public MailAsyncronThread(MailService mailService, String[] multipleTo, String subject, String message){
        this.mailService = mailService;
        this.multipleTo = multipleTo;
        this.subject = subject;
        this.message = message;
        this.multiple = true;
    }

    @Override
    public void run() {
        try {
            if(!this.multiple)
                mailService.SendMail(to, subject, message);
            else 
                mailService.SendMail(multipleTo, subject, message);
        } catch (MessagingException ex) {
            Logger.getLogger(MailAsyncronThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
