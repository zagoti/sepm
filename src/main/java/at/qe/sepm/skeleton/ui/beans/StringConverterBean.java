package at.qe.sepm.skeleton.ui.beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This bean is neccessary to convert multiple data from the database to a
 * printable outputversion for the gui.
 */
@Component
@Scope("session")
public class StringConverterBean {
    
    public String convertPhoneNumber(String digits){
        if(digits.length() < 2) return "";
        if(digits.length() < 6) return "Unültige Nummer gespeichert!";
        StringBuilder phoneBilder = new StringBuilder();
        phoneBilder.append("+(");
        phoneBilder.append(digits.substring(0, 2));
        phoneBilder.append(") ");
        phoneBilder.append(digits.substring(2, 5));
        phoneBilder.append("-");
        phoneBilder.append(digits.substring(5));
        return phoneBilder.toString();
        
    }
}
