package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.services.ImageService;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
 
@Component
@Scope("view")
public class FileUploadController {
        
    @Autowired
    ImageService imageService;
    
    private String id;

    /**
     * Sets current id.
     *
     * @param id
     *            to set the current id to.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Returns current id.
     *
     * @return current id.
     */
    public String getId() {
    	return id;
    }

    /**
     * Save files for children.
     *
     * @param event
     *            the file of the child.
     * @throws Exception
     */
    public void handleFileUploadChild(FileUploadEvent event) throws Exception { 
         if(id == null) return;
         id = id.replace('/', '_').replace('\\','_').replace(' ', '-');
         imageService.saveImageChild(event.getFile().getInputstream(), id);
    }

    /**
     * Save files for parents.
     *
     * @param event
     *            the file of the parents.
     * @throws Exception
     */
    public void handleFileUploadParent(FileUploadEvent event) throws Exception {
        if(id == null) return;
        id = id.replace('/', '_').replace('\\','_').replace(' ', '-');
        imageService.saveImageParent(event.getFile().getInputstream(), id);
    }

    /**
     * Save files for caregiver.
     *
     * @param event
     *            the file of the caregiver.
     * @throws Exception
     */
    public void handleFileUploadCaregiver(FileUploadEvent event) throws Exception {
        if(id == null) return;
        id = id.replace('/', '_').replace('\\','_').replace(' ', '-');
        imageService.saveImageCaregiver(event.getFile().getInputstream(), id);
    }    
}