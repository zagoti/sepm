package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.*;
import at.qe.sepm.skeleton.services.CaregiverService;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;

/**
 * Controller for the caregiver list view.
 *
 */
@Component
@Scope("view")
public class CaregiverListController {

    @Autowired
    private CaregiverService caregiverService;
    @Autowired
    private SessionInfoBean sessionInfoBean;
    @Autowired
    private ChildService childService;
    
    private Collection<Caregiver> filteredCaregivers;

    public Collection<Caregiver> getFilteredCaregivers() {
        return this.filteredCaregivers;
    }

    public void setFilteredCaregivers(Collection<Caregiver> filteredCaregivers) {
        this.filteredCaregivers = filteredCaregivers;
    }

    /**
     * A method which returns a collection of
     * all caregivers.
     *
     * @return Collection of all caregivers
     */
    public Collection<Caregiver> getCaregivers() {
        return caregiverService.loadAllCaregivers();
    }

    /**
     * A method which returns a collection of all caregivers
     * of a parent.
     *
     * @param parent
     *            whose children's caregivers are wanted
     * @return Collection of Person.
     */
    public Collection<Person> getCaregiversOfParent(User parent) {
        Collection<Person> caregivers = new HashSet<>();
        for (Child c : childService.loadChildrenByParent(parent)) {
            caregivers.addAll(c.getCaregivers());
        }
        return caregivers;
    }


    /**
     * A method which returns a collection of all caregivers of
     * the current user.
     *
     * @return Collection of Person.
     */
    public Collection<Person> getCaregiversOfCurrentUser() {
        return getCaregiversOfParent(sessionInfoBean.getCurrentUser());
    }
}
