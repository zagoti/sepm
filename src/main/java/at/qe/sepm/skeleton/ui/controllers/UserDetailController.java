package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.Attendance;
import at.qe.sepm.skeleton.model.Child;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.ChildService;
import at.qe.sepm.skeleton.services.MailService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import at.qe.sepm.skeleton.utils.PasswordGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.context.RequestContext;

/**
 * Controller for the user detail view and modification.
 *
 */
@Component
@Scope("view")
public class UserDetailController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private ChildService childService;

    @Autowired
    private SessionInfoBean sessionInfoBean;
    
    @Autowired
    private AttendanceListController attendanceController;
    
    @Autowired
    private MailService mailService;
    
    private User user;
    
    private Child child;
    
    private boolean userRegistrated;
    
    private boolean changePassword;
    
    private String newPassword;
    
    private String newPasswordCheck;


    /**
     * Get first name of the current user. If it does not exist, create new current user
     *
     * @return 
     */
    public String getFirstName() {
        if(user == null) {
            createUser();
        }
        return user.getFirstName();
    }

    public void setFirstName(String firstName) {     
        user.setFirstName(firstName);
    }

    /**
     * This method is specific for the parent settings, to enable
     * the input field to change the parent password
     * 
     * @param changePassword
     *            as boolean value
     */
    public void setChangePassword(boolean changePassword) {
        this.changePassword = changePassword;
    }

    /**
     * This method is specific for the parent settings, to enable 
     * the input field to change the parent password 
     * 
     * @return true or false
     */
    public boolean isChangePassword() {
        return changePassword;
    }

    /**
     * First set of password
     * 
     * @param password
     *            as String
     */
    public void setNewPassword(String password) {
        newPassword = password;
    }

    /**
     * Get current password
     *
     * @return password as String
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Set current newPasswordCheck. This is required, because the user has to 
     * enter the password twice to change it. With this setter the second attempt
     * is saved.
     * 
     * @param password
     *            as String
     */
    public void setNewPasswordCheck(String password) {
        newPasswordCheck = password;
    }

    /**
     * Get current newPasswordCheck. This is required, because the user has to 
     * enter the password twice to change it. With this getter the second attempt 
     * is returned
     * 
     * @return password as String
     */
    public String getNewPasswordCheck() {
        return newPasswordCheck;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }
    
    public Child getChildById(){
        return this.child;
    }
    
    public void setChildById(String id){
        System.err.println("id: " + id);
        this.child = childService.loadChild(id);
    }
    
    /**
     * Return true if current user is set, return false if no current user is set
     * 
     * @return 
     */
    public boolean isUserRegistrated() {
        return userRegistrated;
    }

    /**
     * Set userRegistrated
     * 
     * @param userRegistrated
     *            as boolean value
     */
    public void setUserRegistrated(boolean userRegistrated) {
        this.userRegistrated = userRegistrated;
    }

    /**
     * Get current user
     * 
     * @return User
     */
    public User getUserById() {
        return this.user;
    }

    /**
     * Set user by id
     * 
     * @param id
     *            as String
     */
    public void setUserById(String id) {
        this.user = userService.loadUser(id);
    }
  
    /**
     * Return true if current user is set, return false if no user is set
     * 
     * @return boolean value
     */
    public boolean isUserSelected() {
        return this.user != null;
    }
    
    public void removeChild(){
        childService.removeParent(child, this.user);
        doSaveParentChanges();
    }

    /**
     * Returns the currently displayed user.
     *
     * @return
     */
    public User getUser() {
        return this.user;
    }

    /**
     * Sets the currently displayed user and reloads it form db. This user is
     * targeted by any further calls of
     * {@link #doReloadUser()}, {@link #doSaveUser()} and
     * {@link #doDeleteUser()}.
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
        //doReloadUser();
    }
    
    /**
     * Action to force a reload of the currently displayed user.
     *
     */
    public void doReloadUser() {
        user = userService.loadUser(user.getId());        
    }
    
    /** 
     * Saves an user object with role parent (only executed once for user)
     *
     */
    public void doSaveParent() {        
        Set<UserRole> roles = new HashSet<>();
        roles.add(UserRole.PARENT);
        this.user.setRoles(roles);
        this.user.setEnabled(true);
       
        GenerateAndSendMail(user);
        doSaveUser();
    }
    
    /**
     * Saves a user which was edited in the gui
     * 
     */
    public void doSaveParentChanges(){
        user = this.userService.saveUser(user);
        RequestContext.getCurrentInstance().execute("PF('configParent').hide()");
    }
    
    /**
     * Saves an user object with role employee (only executed once for user)
     *
     */
    public void doSaveEmployee(){
        Set<UserRole> roles = new HashSet<>();
        roles.add(UserRole.EMPLOYEE);
        this.user.setRoles(roles);
        this.user.setEnabled(true);
        
        GenerateAndSendMail(user);
        this.user.setEnabled(true);
        doSaveUser();
        redirect();
    }
    
    /**
     * Sends the bill viewed in parent-settings to the assigned user.
     * 
     * @param attendances in the time spane
     * @param sum the calculated sum of the prices from the lunch
     */
    public void sendBill(String sum){
        Collection<Attendance> attendances = attendanceController.getAttendancesByParentInTimespan(user.getId());
        if(attendances == null) return;
        StringBuilder sb = new StringBuilder();
        sb.append("Hallo " + user.getFirstName()+",<br/><br/>");
        sb.append("Hiermit übermitteln wir Ihnen die aktuelle Abrechnung:<br/><br/>");
        for(Attendance attendance : attendances){
            sb.append(attendance.getLunch().getMenu() + ": " + String.format("%.2f\u20ac", attendance.getLunch().getPrice()) + "<br/>");
        }      
        sb.append("<br/>Gesamtkosten: " + sum + "<br/>");
        sb.append("Für weitere Informationen bezüglich der Essensabrechnungen wenden Sie sich bitte an eine Mitarbeiter.<br/>");
        sb.append("Mit freundlichen Grüßen<br/>");
        sb.append("ABC Programming Team");
        MailAsyncronThread mail = new MailAsyncronThread(mailService, user.getEmail(), "Neue Aufgabe - ABC Kinderkrippe", sb.toString());
            
            
            Thread thread = new Thread(mail);
            thread.start();                
    }
    
    /**
     * This method generates a random password and
     * sends it via email to the user.
     *
     */
    public void GenerateAndSendMail(User user) {
    	// generate password and send mail
        
        PasswordGenerator pg = new PasswordGenerator();
        String pass = pg.getPassword().substring(0, 8);
        user.setPassword(new BCryptPasswordEncoder().encode(pass));
        
        StringBuilder sb = new StringBuilder();
        sb.append("Hallo " + user.getFirstName()+",<br/><br/>");
        sb.append("Ihre Zugangsdaten lauten wie folgt:<br/><br/>");
        sb.append(user.getEmail() + "<br/>");
        sb.append(pass + "<br/><br/>");
        sb.append("Hinweis: Bitte ändern Sie Ihr Passwort unverzüglich!<br/>");
        sb.append("Falls Sie irgendwelche Probleme mit der Anmeldung haben, ");
        sb.append("dann tut es uns leid, aber so ist das Leben. Das Leben ist nicht immer fair.<br/><br/>");
        sb.append("Mit freundlichen Grüßen<br/>");
        sb.append("ABC Programming Team");
        
       MailAsyncronThread mail = new MailAsyncronThread(mailService, user.getEmail(), "Wilkommen, Ihre Zugangsdaten - ABC Kinderkrippe", sb.toString());
            
            
            Thread thread = new Thread(mail);
            thread.start();
    }
    
    public void redirect(){
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./employee-settings.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(UserDetailController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Add the current child from the childController to the current user
     *
     */
    public void addChildToParent(){   
        if(this.child != null && this.user != null){
            user.addChild(child);
            userService.saveUser(user);
        }
    }
    
    /**
     * Action to save the currently displayed user.
     *
     */
    public void doSaveUser() {      
        if(newPassword != null && newPasswordCheck != null) {
            if(newPassword.equals(newPasswordCheck)) {                
                user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
            }
        }
        if(user.getRoles().contains(UserRole.PARENT) && (user.getChildren() == null || user.getChildren().isEmpty())) {
        	user.setEnabled(false);
        	userService.saveUser(user);
        }
        user = this.userService.saveUser(user);
        userRegistrated = true;
    }
    
    /*
     * Action to save the currently displayed employee and redircts.
     * 
     */
    public void doUpdateEmployee() {
        doSaveUser();
        redirect();
    }

    public void createUser() {
        this.user = new User();
    }
    
    /**
     * Action to delete the currently displayed user.
     *
     */
    public void doDeleteUser() {
        this.userService.deleteUser(user);
        user = null;
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./employee-settings.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ChildCreateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setCurrentUser() {
        this.user = sessionInfoBean.getCurrentUser();
    }
    
    /**
     * Return true if the current user is the logged in user, false if not
     *
     * @return 
     */
    public boolean isLoggedinUser(){
        if(this.user == null) return false;
        return this.user.getId().equals(sessionInfoBean.getCurrentUser().getId());
    }
}
